/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : db_lookall

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-11-23 17:16:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tblbarang
-- ----------------------------
DROP TABLE IF EXISTS `tblbarang`;
CREATE TABLE `tblbarang` (
`id_brg`  int(11) NOT NULL AUTO_INCREMENT ,
`id_toko`  int(11) NOT NULL ,
`id_ktg`  int(11) NOT NULL ,
`nama_brg`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`foto_brg`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`warna_brg`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`ukuran_brg`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`stok_brg`  int(11) NULL DEFAULT NULL ,
`sku_brg`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`harga_brg`  decimal(20,2) NULL DEFAULT NULL ,
`deskripsi_brg`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
PRIMARY KEY (`id_brg`),
INDEX `id_toko` (`id_toko`) USING BTREE ,
INDEX `id_ktg` (`id_ktg`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=5

;

-- ----------------------------
-- Records of tblbarang
-- ----------------------------
BEGIN;
INSERT INTO `tblbarang` VALUES ('1', '1', '3', 'Kaos Oblong HQ', 'bahan/upload/barang/89955_02-thumbnail.jpg', 'hitam', 'L', '88', '123/qwe/ads/2019', '80000.00', 'Kaos merek oblong kekinian Hight Quality import'), ('2', '1', '2', 'bb', 'bahan/upload/barang/86617_Screen Shot 2019-10-31 at 8.49.13 PM.png', 'biru', 'xl', '2', 'hui', '60000.00', 'bb'), ('3', '2', '2', 'baju', 'bahan/upload/barang/80367_2.png', 'hitam', 'm', '5', 'sc12', '90000.00', 'bahan ok'), ('4', '3', '2', 'baju', 'bahan/upload/barang/98593_Screen Shot 2019-10-31 at 8.57.58 PM.png', 'putih', 'm', '4', 'nc12', '100000.00', 'ok');
COMMIT;

-- ----------------------------
-- Table structure for tbldetailpemesanan
-- ----------------------------
DROP TABLE IF EXISTS `tbldetailpemesanan`;
CREATE TABLE `tbldetailpemesanan` (
`id_detailpemesanan`  int(11) NOT NULL AUTO_INCREMENT ,
`id_pembeli`  int(11) NOT NULL ,
`id_brg`  int(11) NOT NULL ,
`id_pemesanan`  int(11) NOT NULL ,
`quantity`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id_detailpemesanan`),
INDEX `id_brg` (`id_brg`) USING BTREE ,
INDEX `id_pemesanan` (`id_pemesanan`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=8

;

-- ----------------------------
-- Records of tbldetailpemesanan
-- ----------------------------
BEGIN;
INSERT INTO `tbldetailpemesanan` VALUES ('1', '1', '1', '1', '4'), ('2', '1', '1', '2', '5'), ('3', '1', '1', '3', '1'), ('4', '1', '1', '3', '2'), ('5', '1', '3', '4', '2'), ('6', '1', '4', '5', '2'), ('7', '1', '2', '6', '1');
COMMIT;

-- ----------------------------
-- Table structure for tblkategori
-- ----------------------------
DROP TABLE IF EXISTS `tblkategori`;
CREATE TABLE `tblkategori` (
`id_ktg`  int(11) NOT NULL AUTO_INCREMENT ,
`nama_ktg`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id_ktg`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=6

;

-- ----------------------------
-- Records of tblkategori
-- ----------------------------
BEGIN;
INSERT INTO `tblkategori` VALUES ('1', 'Sepatu'), ('2', 'Baju'), ('3', 'Kaos'), ('4', 'Jaket'), ('5', 'Tas');
COMMIT;

-- ----------------------------
-- Table structure for tblkeranjang
-- ----------------------------
DROP TABLE IF EXISTS `tblkeranjang`;
CREATE TABLE `tblkeranjang` (
`id_keranjang`  int(11) NOT NULL AUTO_INCREMENT ,
`id_pembeli`  int(11) NOT NULL ,
`id_brg`  int(11) NOT NULL ,
`quantity`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id_keranjang`),
INDEX `id_brg` (`id_brg`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of tblkeranjang
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tblpembeli
-- ----------------------------
DROP TABLE IF EXISTS `tblpembeli`;
CREATE TABLE `tblpembeli` (
`id_pembeli`  int(11) NOT NULL AUTO_INCREMENT ,
`nama_pembeli`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`email_pembeli`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`kelamin_pembeli`  enum('laki-laki','perempuan') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`foto_pembeli`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`username`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`password`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`nohp_pembeli`  varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`tgl_bergabung`  date NULL DEFAULT NULL ,
PRIMARY KEY (`id_pembeli`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Records of tblpembeli
-- ----------------------------
BEGIN;
INSERT INTO `tblpembeli` VALUES ('1', 'Bambang Triatmojo xxx', 'bambang@gmail.com', 'laki-laki', 'bahan/upload/pembeli/20148_2.jpg', 'bambang', '202cb962ac59075b964b07152d234b70', '82225614356', '2019-10-31'), ('2', 'andik firdaus', 'andif@gmail.com', 'laki-laki', null, 'andik', '202cb962ac59075b964b07152d234b70', '8987178688', '2019-11-01');
COMMIT;

-- ----------------------------
-- Table structure for tblpemesanan
-- ----------------------------
DROP TABLE IF EXISTS `tblpemesanan`;
CREATE TABLE `tblpemesanan` (
`id_pemesanan`  int(11) NOT NULL AUTO_INCREMENT ,
`id_pembeli`  int(11) NULL DEFAULT NULL ,
`totalharga_pemesanan`  decimal(20,2) NULL DEFAULT NULL ,
`tanggal_pemesanan`  date NULL DEFAULT NULL ,
`status_pemesanan`  enum('dibatalkan','sudah diambil','menunggu konfirmasi','menunggu diambil') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id_pemesanan`),
FOREIGN KEY (`id_pembeli`) REFERENCES `tblpembeli` (`id_pembeli`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `xpembeli` (`id_pembeli`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=7

;

-- ----------------------------
-- Records of tblpemesanan
-- ----------------------------
BEGIN;
INSERT INTO `tblpemesanan` VALUES ('1', '1', '320000.00', '2019-11-17', 'dibatalkan'), ('2', '1', '400000.00', '2019-11-17', 'dibatalkan'), ('3', '1', '240000.00', '2019-11-18', 'dibatalkan'), ('4', '1', '180000.00', '2019-11-18', 'dibatalkan'), ('5', '1', '200000.00', '2019-11-18', 'menunggu konfirmasi'), ('6', '1', '60000.00', '2019-11-18', 'menunggu konfirmasi');
COMMIT;

-- ----------------------------
-- Table structure for tbltoko
-- ----------------------------
DROP TABLE IF EXISTS `tbltoko`;
CREATE TABLE `tbltoko` (
`id_toko`  int(11) NOT NULL AUTO_INCREMENT ,
`nama_toko`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`email_toko`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`foto_toko`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`alamat_toko`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
`koordinat_toko`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`wa_toko`  varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`ig_toko`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`username`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`password`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`tgl_bergabung`  date NULL DEFAULT NULL ,
PRIMARY KEY (`id_toko`),
INDEX `id_toko` (`id_toko`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=4

;

-- ----------------------------
-- Records of tbltoko
-- ----------------------------
BEGIN;
INSERT INTO `tbltoko` VALUES ('1', 'subur jaya', 'suburjaya@gmail.com', 'bahan/upload/toko/5030_3.jpg', 'jl. qwertyuiop asdfghjkl subur jaya', '-7.783281641641332,110.39729366162112', '82225656833', 'subur_jaya', 'subur', '202cb962ac59075b964b07152d234b70', '2019-11-01'), ('2', 'starcross', 'starcross@gmail.com', 'bahan/upload/toko/41269_starcross.png', 'jalan kota gedhe', '-7.781921,110.36467800000003', '082233228898', 'starcross', 'sc', '202cb962ac59075b964b07152d234b70', '2019-11-18'), ('3', 'nimco', 'nimco@gmail.com', 'bahan/upload/toko/40636_nimco.png', 'jalan sokonandi', '-17.7819210000000005,110.36570796826173', '0811111111', 'nimco', 'nimco', '202cb962ac59075b964b07152d234b70', '2019-11-18');
COMMIT;

-- ----------------------------
-- Table structure for tbluser
-- ----------------------------
DROP TABLE IF EXISTS `tbluser`;
CREATE TABLE `tbluser` (
`tbluser_id`  int(11) NOT NULL AUTO_INCREMENT ,
`username`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`password`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`tbluser_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of tbluser
-- ----------------------------
BEGIN;
INSERT INTO `tbluser` VALUES ('1', 'admin', '202cb962ac59075b964b07152d234b70');
COMMIT;

-- ----------------------------
-- Auto increment value for tblbarang
-- ----------------------------
ALTER TABLE `tblbarang` AUTO_INCREMENT=5;

-- ----------------------------
-- Auto increment value for tbldetailpemesanan
-- ----------------------------
ALTER TABLE `tbldetailpemesanan` AUTO_INCREMENT=8;

-- ----------------------------
-- Auto increment value for tblkategori
-- ----------------------------
ALTER TABLE `tblkategori` AUTO_INCREMENT=6;

-- ----------------------------
-- Auto increment value for tblkeranjang
-- ----------------------------
ALTER TABLE `tblkeranjang` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for tblpembeli
-- ----------------------------
ALTER TABLE `tblpembeli` AUTO_INCREMENT=3;

-- ----------------------------
-- Auto increment value for tblpemesanan
-- ----------------------------
ALTER TABLE `tblpemesanan` AUTO_INCREMENT=7;

-- ----------------------------
-- Auto increment value for tbltoko
-- ----------------------------
ALTER TABLE `tbltoko` AUTO_INCREMENT=4;

-- ----------------------------
-- Auto increment value for tbluser
-- ----------------------------
ALTER TABLE `tbluser` AUTO_INCREMENT=2;
