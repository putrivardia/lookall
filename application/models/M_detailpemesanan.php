<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_detailpemesanan extends CI_Model {
  var $table = 'tbldetailpemesanan';

  public function __construct()
  {
    parent::__construct();
  }

  public function ambil_semua()
  {
    $this->db->order_by('id_detailpemesanan', 'DESC');
    $query = $this->db->get($this->table);
    $result = $query->result_array();

    return $result;
  }

  public function ambil_semua_by_id($id)
  {
    $this->db->select('tbldetailpemesanan.*,tblbarang.nama_brg,tblbarang.warna_brg,tblbarang.ukuran_brg,tblbarang.sku_brg,tblbarang.harga_brg,tblbarang.foto_brg,tblpemesanan.status_pemesanan');
    $this->db->join('tblbarang', 'tblbarang.id_brg = tbldetailpemesanan.id_brg');
    $this->db->join('tblpemesanan', 'tblpemesanan.id_pemesanan= tbldetailpemesanan.id_pemesanan');
    $this->db->order_by('id_detailpemesanan', 'DESC');
    $this->db->where('tbldetailpemesanan.id_pembeli',$id);
    $this->db->where('tblpemesanan.status_pemesanan',"menunggu konfirmasi");
    $query = $this->db->get($this->table);
    $result = $query->result_array();

    return $result;
  }

  public function ambil_semua_by_id_riwayat($id)
  {
    $this->db->select('tbldetailpemesanan.*,tblbarang.nama_brg,tblbarang.warna_brg,tblbarang.ukuran_brg,tblbarang.sku_brg,tblbarang.harga_brg,tblbarang.foto_brg,tblpemesanan.status_pemesanan');
    $this->db->join('tblbarang', 'tblbarang.id_brg = tbldetailpemesanan.id_brg');
    $this->db->join('tblpemesanan', 'tblpemesanan.id_pemesanan= tbldetailpemesanan.id_pemesanan');
    $this->db->order_by('id_detailpemesanan', 'DESC');
    $this->db->where('tbldetailpemesanan.id_pembeli',$id);
    $query = $this->db->get($this->table);
    $result = $query->result_array();

    return $result;
  }

  public function ambil_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_detailpemesanan',$id);
    $query = $this->db->get();

    return $query->row();
  }

  public function simpan($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function perbarui($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function hapus_by_id($id)
  {
    $this->db->where('id_detailpemesanan', $id);
    $this->db->delete($this->table);
  }

}