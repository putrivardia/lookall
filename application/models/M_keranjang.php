<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_keranjang extends CI_Model {
  var $table = 'tblkeranjang';

  public function __construct()
  {
    parent::__construct();
  }

  public function ambil_semua()
  {
    $this->db->order_by('id_keranjang', 'DESC');
    $query = $this->db->get($this->table);
    $result = $query->result_array();

    return $result;
  }

  public function ambil_semua_by_id($id)
  {
    $this->db->select('tblkeranjang.*,tblbarang.nama_brg,tblbarang.warna_brg,tblbarang.ukuran_brg,tblbarang.sku_brg,tblbarang.harga_brg,tblbarang.foto_brg');
    $this->db->join('tblbarang', 'tblbarang.id_brg = tblkeranjang.id_brg');
    $this->db->order_by('id_keranjang', 'DESC');
    $this->db->where('id_pembeli',$id);
    $query = $this->db->get($this->table);
    $result = $query->result_array();

    return $result;
  }

  public function ambil_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_keranjang',$id);
    $query = $this->db->get();

    return $query->row();
  }

  public function simpan($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function perbarui($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function hapus_by_id($id)
  {
    $this->db->where('id_keranjang', $id);
    $this->db->delete($this->table);
  }

  public function cek_login($tbl_pengguna, $where)
  { 
    return $this->db->get_where($tbl_pengguna, $where);
  }

  public function get_user_data($tbl_pengguna, $username)
  {
    $this->db->from($tbl_pengguna);
    $this->db->where('username', $username);
    $query = $this->db->get();

    return $query->row();
  }

}