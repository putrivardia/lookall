<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_pemesanan extends CI_Model {
  var $table = 'tblpemesanan';

  public function __construct()
  {
    parent::__construct();
  }

  public function ambil_semua()
  {
    $this->db->order_by('id_pemesanan', 'DESC');
    $query = $this->db->get($this->table);
    $result = $query->result_array();

    return $result;
  }

  public function ambilterakhir()
  {
    $maxid = 0;
    $row = $this->db->query('SELECT MAX(id_pemesanan) AS id_pemesanan FROM tblpemesanan')->row();
    if ($row) {
        $maxid = $row->id_pemesanan; 
    }
    return $maxid;
  }

  public function ambil_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_pemesanan',$id);
    $query = $this->db->get();

    return $query->row();
  }

  public function simpan($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function perbarui($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function hapus_by_id($id)
  {
    $this->db->where('id_pemesanan', $id);
    $this->db->delete($this->table);
  }

}