<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

  public function __construct()
  {
      parent::__construct();
  }

  public function cek_login($tbl_pengguna, $where)
  {	
		return $this->db->get_where($tbl_pengguna, $where);
  }

  public function get_user_data($tbl_pengguna, $username)
  {
		$this->db->from($tbl_pengguna);
		$this->db->where('username', $username);
		$query = $this->db->get();

		return $query->row();
  }

}
