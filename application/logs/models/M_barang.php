<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_barang extends CI_Model {
  var $table = 'tblbarang';

  public function __construct()
  {
    parent::__construct();
  }

  public function ambil_semua()
  {
    $this->db->order_by('id_brg', 'DESC');
    $query = $this->db->get($this->table);
    $result = $query->result_array();

    return $result;
  }

  public function ambil_by_toko($toko)
  {
    $this->db->select('tblbarang.*,tblkategori.nama_ktg');
    $this->db->join('tblkategori', 'tblkategori.id_ktg = tblbarang.id_ktg');
    $this->db->order_by('id_brg', 'DESC');
    $this->db->where('id_toko',$toko);
    $query = $this->db->get($this->table);
    $result = $query->result_array();

    return $result;
  }

  public function ambil_by_id($id)
  {
    $this->db->select('tblbarang.*,tblkategori.nama_ktg');
    $this->db->join('tblkategori', 'tblkategori.id_ktg = tblbarang.id_ktg');
    $this->db->where('id_brg',$id);
    $query = $this->db->get($this->table);

    return $query->row();
  }

  public function simpan($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function perbarui($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function hapus_by_id($id)
  {
    $this->db->where('id_brg', $id);
    $this->db->delete($this->table);
  }

  public function cek_login($tbl_pengguna, $where)
  { 
    return $this->db->get_where($tbl_pengguna, $where);
  }

  public function get_user_data($tbl_pengguna, $username)
  {
    $this->db->from($tbl_pengguna);
    $this->db->where('username', $username);
    $query = $this->db->get();

    return $query->row();
  }

}