<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_toko extends CI_Model {
  var $table = 'tbltoko';

  public function __construct()
  {
    parent::__construct();
  }

  public function ambil_semua()
  {
    $this->db->order_by('id_toko', 'DESC');
    $query = $this->db->get($this->table);
    $result = $query->result_array();

    return $result;
  }

  public function ambil_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_toko',$id);
    $query = $this->db->get();

    return $query->row();
  }

  public function simpan($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function perbarui($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function hapus_by_id($id)
  {
    $this->db->where('id_toko', $id);
    $this->db->delete($this->table);
  }

  public function cek_login($tbl_pengguna, $where)
  { 
    return $this->db->get_where($tbl_pengguna, $where);
  }

  public function get_user_data($tbl_pengguna, $username)
  {
    $this->db->from($tbl_pengguna);
    $this->db->where('username', $username);
    $query = $this->db->get();

    return $query->row();
  }

}