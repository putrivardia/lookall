<?php $this->load->view('header'); ?>



  <!-- MASUK -->
  <section class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 style="margin-top:10px;" class="section-heading text-uppercase">MASUK</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form id="form" name="form" novalidate="novalidate" method="POST" action="<?php echo base_url()?>masuk/login">
            <div class="row">
              <div class="col-md-4">
               
              </div>
             <div class="col-md-4"> 
                <?php if($this->session->flashdata('msg')): ?>
                    <div class="alert alert-<?php echo $this->session->flashdata('code'); ?>"><?php echo $this->session->flashdata('msg'); ?></div>
                <?php endif; ?>                

                <div class="form-group">
                  <label style="color: white;">Username :</label>
                  <input class="form-control" id="username" name="username" type="text" placeholder="Nama Pengguna *" required="required" data-validation-required-message="Nama Pengguna">
                  <p class="help-block text-danger"></p>
                </div>
                
                <div class="form-group">
                  <label style="color: white;">Kata Sandi :</label>
                  <input class="form-control" id="katasandi" name="katasandi" type="password" placeholder="Kata Sandi*" required="required" data-validation-required-message="Kata Sandi.">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center">
                <div id="success"></div>
                <button class="btn btn-primary btn-l text-uppercase" type="submit">Masuk</a>
              </div> 
               <div class="col-lg-12 text-center">
              &nbsp;<p  style="color: white;">atau daftar </p>&nbsp;
            </div>
              <div class="col-lg-12 text-center">
                <div id="success"></div> 
                <a href="<?php echo base_url(); ?>konsumen_daftar"  style="margin-top: -10px"  class="btn btn-primary btn-l text-uppercase" type="submit">Pembeli</a>&nbsp;&nbsp;&nbsp;
                <a href="<?php echo base_url(); ?>toko_daftar"  style="margin-top: -10px"  class="btn btn-primary btn-l text-uppercase" type="submit">Penjual</a>
              </div>

            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer class="footer">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-4">
          <span class="copyright">Copyright &copy; Your Website 2019</span>
        </div>
        <div class="col-md-4">
          <ul class="list-inline social-buttons">
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
           
          </ul>
        </div>
        <div class="col-md-4">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item">
              <a href="#">Privacy Policy</a>
            </li>
            <li class="list-inline-item">
              <a href="#">Terms of Use</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  

<?php $this->load->view('footer'); ?>
