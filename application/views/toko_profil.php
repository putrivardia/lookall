<?php $this->load->view('header'); ?>



  <!-- form daftar -->
  <section style="margin-top: 25px; " class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
            <?php if ($toko['foto_toko']<>''){ ?>
            <center>
              <img class="lazy" alt="" src="<?php echo base_url(); ?><?php echo $toko['foto_toko'] ?>" style="display: block;width: 155px; height: 155px;border-radius:50%;" >
            </center><br>
              
            <?php } else { ?>
            <center>
              <img class="lazy" alt="" src="<?php echo base_url(); ?>bahan/upload/images.png" style="display: block;width: 155px; height: 155px;border-radius:50%;" >
            </center><br>
            <?php } ?>
          <h3 class="section-heading text-uppercase"> <?php echo $toko['nama_toko'] ?></h3><br>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form id="contactForm" name="sentMessage" novalidate="novalidate">
            <div class="row justify-content-center" >
              <div class="col-md-8">
                  <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                          <label style="color: white;">Email Toko</label>
                        </div>
                        <div class="col-md-6">
                          <label style="color: white;">: &nbsp;&nbsp;<?php echo $toko['email_toko'] ?></label>
                        </div>
                    </div>
                  </div>
              
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label style="color: white;">Alamat Toko</label>
                      </div>
                      <div class="col-md-6">
                        <label style="color: white;">: &nbsp;&nbsp;<?php echo $toko['alamat_toko'] ?></label>
                      </div> 
                    </div>
                  </div> 

                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label style="color: white;">Instagram Toko</label>
                      </div>
                      <div class="col-md-6">
                        <label style="color: white;">: &nbsp;&nbsp;<?php echo $toko['ig_toko'] ?></label>
                      </div>  
                    </div> 
                  </div> 

                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label style="color: white;">Whatsapp Toko</label>
                      </div>
                      <div class="col-md-6">
                        <label style="color: white;">: &nbsp;&nbsp;<?php echo $toko['wa_toko'] ?></label>
                      </div>   
                    </div>
                  </div>

                  <div class="form-group">
                     <div class="row">
                      <div class="col-md-6">
                        <label style="color: white;">Username Toko</label>
                      </div>
                      <div class="col-md-6">
                        <label style="color: white;">: &nbsp;&nbsp;<?php echo $toko['ig_toko'] ?></label>
                      </div> 
                     </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label style="color: white;">Kata Sandi Toko</label>
                      </div>
                      <div class="col-md-6">
                        <label style="color: white;">: &nbsp;&nbsp;********</label>
                      </div>  
                    </div>
                  </div>

                  <div class="form-group">
                      <div class="row">
                        <div class="col-md-6">
                          <label style="color: white;">Koordinat Toko</label>
                        </div>
                        <div class="col-md-6">
                          <label style="color: white;">: &nbsp;&nbsp;<?php echo $toko['koordinat_toko'] ?></label>
                        </div> 
                      </div> 
                  </div>

                  <?php 
                    $koor  = $toko['koordinat_toko'];
                    $set = explode(",", $koor);
                    $koorlong = $set[0];
                    $koorlat = $set[1];
                  ?>

                  <div class="form-group">
                    <div class="row">
                      <div class="mapouter"><div class="gmap_canvas"><iframe width="800px" height="350px" id="gmap_canvas" src="https://maps.google.com/maps?q=<?php echo $toko['koordinat_toko'] ?>&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{position:relative;text-align:right;height:350px;width:800px;}.gmap_canvas {overflow:hidden;background:none!important;height:350px;width:800px;}</style></div>
                    </div>
                  </div>


              <div class="form-group">
                  <center><button type="button" class="btn btn-primary btn-l text-uppercase" onclick="edit()">Ubah</button></center><br><br><br>

                  <!-- <a href="<?php echo base_url(); ?>toko_pemesanan" id="button_tokopemesanan" class="btn btn-primary btn-l text-uppercase" type="submit">Pemesanan</a>

                  <a href="<?php echo base_url(); ?>toko_tambahproduk" id="button_tokotambahproduk" class="btn btn-primary btn-l text-uppercase" type="submit">Tambah Barang</a>
                  
                  <a href="<?php echo base_url(); ?>toko_laporan" id="button_tokolaporan" class="btn btn-primary btn-l text-uppercase" type="submit">Laporan Toko</a>  -->
              </div>
              </div>

              </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  

<?php $this->load->view('header'); ?>


  <!-- Form Edit toko-->
  <div class="modal fade" id="modal_edt_toko">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h2 class="text-uppercase">Form Edit Toko</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="container">
          <div class="modal-body form">
            <form id="form" class="form">
              <div class="form-body">
                  <input id="id_toko" name="id_toko" value="<?php echo $toko['id_toko'] ?>" type="hidden">
                <div class="form-group">
                  <label >Nama Toko:</label>
                  <input class="form-control" id="nama_toko" name="nama_toko" value="<?php echo $toko['nama_toko'] ?>" type="text" placeholder="Nama Lengkap *" required="required" data-validation-required-message=" Masukan Nama Lengkap">
                  <p class="help-block text-danger"></p>
                </div>

                  <div class="form-group">
                  <label >Email Toko :</label>
                  <input class="form-control" id="email_toko" name="email_toko" value="<?php echo $toko['email_toko'] ?>" type="text" placeholder="Email *" required="required" data-validation-required-message="Masukan Email">
                  <p class="help-block text-danger"></p>
                </div>
              
                     <div class="form-group">
                  <label >Alamat Toko:</label>
                  <textarea class="form-control" id="alamat_toko" name="alamat_toko" placeholder=" Alamat *" required="required" data-validation-required-message="Masukan Alamat."><?php echo $toko['alamat_toko'] ?></textarea>
                  <p class="help-block text-danger"></p>
                </div>

                <label >Titik Lokasi Toko :</label>
               <div class="form-group input-group">
                  <input  type="text" class="form-control" id="koordinat_toko" name="koordinat_toko" value="<?php echo $toko['koordinat_toko'] ?>" placeholder="Isikan Titik Lokasi *">
                  <span class="input-group-btn">
                    <button type="button" onclick="loadthemappicker()" class="btn btn-info btn-flat">Pilih Lokasi</button>
                  </span>
                </div> 

                <div class="form-group">
                  <label >Instagram :</label>
                  <input class="form-control" id="ig_toko" name="ig_toko" value="<?php echo $toko['ig_toko'] ?>" type="text" placeholder="instagram toko *" required="required" data-validation-required-message=" Masukan Nama Lengkap">
                  <p class="help-block text-danger"></p>
                </div> 

                 <div class="form-group">
                  <label >Whatsapp :</label>
                  <input class="form-control" id="wa_toko" name="wa_toko" value="<?php echo $toko['wa_toko'] ?>" type="text" placeholder="whatsapp toko*" required="required" data-validation-required-message=" Masukan Nama Lengkap">
                  <p class="help-block text-danger"></p>
                </div>

                 <div class="form-group">
                  <label >Username :</label>
                  <input class="form-control" id="username" name="username" value="<?php echo $toko['username'] ?>" type="text" placeholder="Username *" required="required" data-validation-required-message="Masukan Username">
                  <p class="help-block text-danger"></p>
                </div>

                <div class="form-group">
                  <label >Password :</label>
                  <input class="form-control" id="password" name="password" type="password" placeholder="*****">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label for="foto_file">Image</label><br />
                  <input id="foto_toko" name="foto_toko" type="hidden">
                  <input id="foto_file" name="file" type="file" multiple>
                  <div id="foto_file-preview">
                    <img src="<?php echo base_url(); ?><?php echo $toko['foto_toko'] ?>" style="margin-top: 20px;" width="300" />
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              </div>
            </form>
        </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Form Edit konsumen-->
  <div class="modal fade" id="modallokasi">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h2 class="text-uppercase">Form Edit Akunku</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="container">
          <div class="modal-body form">
              <div class="alert alert-warning">Peta dibawah tidak tampil ? coba klik <button class="btn btn-warning btn-sm" onclick="loadthemappicker()">Load Peta Ulang</button></div>
              Koordinat : <p id="koordinat"></p>
              <div id="map" style="height: 300px!important;"></div>

              <div class="modal-footer">
                <button type="button" class="btn btn-success btn-l" data-dismiss="modal">
                    <i class="fa fa-check"></i> Pilih Lokasi
                </button>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  

<?php $this->load->view('footer'); ?>

<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyDya4920f_VwXfZcxkSmtsxDEgYT_pI-b8&sensor=false"></script>


<script type="text/javascript">

var koordinattoko = '<?php echo $toko['koordinat_toko'] ?>';
var res = koordinattoko.split(",");
var long=res[0];
var lat =res[1];
var fototoko = '<?php echo $toko['foto_toko'] ?>';
window.baseUrl = '<?php echo base_url(); ?>';
$('#foto_toko').val(fototoko);

$(function () {
    'use strict';
    var url_foto = "<?php echo site_url('toko_profil/upload_foto')?>";
    $('#foto_file').fileupload({
        url: url_foto,
        dataType: 'json',
        done: function (e, data) {
            window.foto = data.result.file;
            var imgPrev = '<img src="'+window.baseUrl+'/'+window.foto+'" style="margin-top: 20px;" width="300" />';
            $("#foto_file-preview").html(imgPrev);
            $("#foto_toko").val(window.foto);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function loadthemappicker()
{
    $('#modallokasi').modal('show');
    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 12,
    center: new google.maps.LatLng(long,lat),
     mapTypeId: google.maps.MapTypeId.ROADMAP
      });
    //posisi awal marker   
    var latLng = new google.maps.LatLng(long,lat);
     
    /* buat marker yang bisa di drag lalu 
      panggil fungsi updateMarkerPosition(latLng)
     dan letakan posisi terakhir di id=latitude dan id=longitude
     */
    var marker = new google.maps.Marker({
        position : latLng,
        title : 'lokasi',
        map : map,
        draggable : true
      });
       
    updateMarkerPosition(latLng);
    google.maps.event.addListener(marker, 'drag', function() {
     // ketika marker di drag, otomatis nilai latitude dan longitude
     //menyesuaikan dengan posisi marker 
        updateMarkerPosition(marker.getPosition());
    });
}

function updateMarkerPosition(latLng) {
  $("#koordinat").html([latLng.lat()]+','+[latLng.lng()]);
  $("#koordinat_toko").val([latLng.lat()]+','+[latLng.lng()]);
}

function edit()
{
  $('#modal_edt_toko').modal('show'); 
}

$("#btnSave").click(function() {
    $("#form").submit();
});

$("#form").validate({
    rules: {
        nama_toko: "required",
        email_toko: "required",
        alamat_toko: "required",
        koordinat_toko: "required",
        ig_toko: "required",
        wa_toko: "required",
        foto_toko: "required",
        username: "required"
    },
    messages: {
        nama_toko: "Mohon isikan nama toko anda",
        email_toko: "Mohon isikan email toko anda",
        alamat_toko: "Mohon isikan alamat toko anda",
        koordinat_toko: "Mohon isikan titik lokasi anda",
        ig_toko: "Mohon isikan instagram toko anda",
        wa_toko: "Mohon isikan nomor whatsapp toko anda",
        foto_toko: "Mohon isikan foto profil toko anda",
        username: "Mohon isikan username login toko anda"
    },
    submitHandler: function() {
      simpan();
    }
});

function simpan()
{
    $('#btnSave').text('menyimpan...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url = "<?php echo site_url('toko_profil/ajax_perbarui')?>";
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data Berhasil disimpan',
                showConfirmButton: false,
                timer: 2000
              })
              setTimeout(function() {
                location.reload();
              }, 2000);
            }

            $('#btnSave').text('Daftar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          Swal.fire({
            position: 'center',
            type: 'error',
            title: 'Oops...',
            text: 'Error menambahkan data',
            timer: 5000
          })
          $('#btnSave').text('Daftar'); //change button text
          $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}
</script>

