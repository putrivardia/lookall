<?php $this->load->view('header'); ?>
  <!--laporan Grid -->
  <section class="bg-light page-section" id="portfolio">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Laporan Toko</h2>
       </div>
      </div>
      <div class="row">
        <table id="example" class="table table-striped table-bordered">
          <thead>
              <tr>
                  <th>No</th>
                  <th>TGL</th>
                  <th>Nama Pemesan</th>
                  <th>Kategori Produk</th>
                  <th>Nama Produk</th>
                  <th>Ukuran</th>
                  <th>Warna</th>
                  <th>Jumlah Produk</th>
                  <th>Total Harga</th> 
                  <th>Status</th> 
                  <th>Aksi (konfirmasi, diambil, batalkan)</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <td>Tiger Nixon</td>
                  <td>System Architect</td>
                  <td>Edinburgh</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2011/04/25</td>
                  <td>$320,800</td>
              </tr>
              <tr>
                  <td>Garrett Winters</td>
                  <td>Accountant</td>
                  <td>Tokyo</td>
                  <td>63</td>
                  <td>2011/07/25</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>$170,750</td>
              </tr>
              <tr>
                  <td>Ashton Cox</td>
                  <td>Junior Technical Author</td>
                  <td>San Francisco</td>
                  <td>66</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2009/01/12</td>
                  <td>$86,000</td>
              </tr>
              <tr>
                  <td>Cedric Kelly</td>
                  <td>Senior Javascript Developer</td>
                  <td>Edinburgh</td>
                  <td>22</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2012/03/29</td>
                  <td>$433,060</td>
              </tr>
              <tr>
                  <td>Airi Satou</td>
                  <td>Accountant</td>
                  <td>Tokyo</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>33</td>
                  <td>2008/11/28</td>
                  <td>$162,700</td>
              </tr>
              <tr>
                  <td>Brielle Williamson</td>
                  <td>Integration Specialist</td>
                  <td>New York</td>
                  <td>61</td>
                  <td>2012/12/02</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>$372,000</td>
              </tr>
              <tr>
                  <td>Herrod Chandler</td>
                  <td>Sales Assistant</td>
                  <td>San Francisco</td>
                  <td>59</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2012/08/06</td>
                  <td>$137,500</td>
              </tr>
              <tr>
                  <td>Rhona Davidson</td>
                  <td>Integration Specialist</td>
                  <td>Tokyo</td>
                  <td>55</td>
                  <td>2010/10/14</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>$327,900</td>
              </tr>
              <tr>
                  <td>Colleen Hurst</td>
                  <td>Javascript Developer</td>
                  <td>San Francisco</td>
                  <td>39</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2009/09/15</td>
                  <td>$205,500</td>
              </tr>
              <tr>
                  <td>Sonya Frost</td>
                  <td>Software Engineer</td>
                  <td>Edinburgh</td>
                  <td>23</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2008/12/13</td>
                  <td>$103,600</td>
              </tr>
              <tr>
                  <td>Jena Gaines</td>
                  <td>Office Manager</td>
                  <td>London</td>
                  <td>30</td>
                  <td>2008/12/19</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>$90,560</td>
              </tr>
              <tr>
                  <td>Quinn Flynn</td>
                  <td>Support Lead</td>
                  <td>Edinburgh</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>22</td>
                  <td>2013/03/03</td>
                  <td>$342,000</td>
              </tr>
              <tr>
                  <td>Charde Marshall</td>
                  <td>Regional Director</td>
                  <td>San Francisco</td>
                  <td>36</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2008/10/16</td>
                  <td>$470,600</td>
              </tr>
              <tr>
                  <td>Haley Kennedy</td>
                  <td>Senior Marketing Designer</td>
                  <td>London</td>
                  <td>43</td>
                  <td>2012/12/18</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>$313,500</td>
              </tr>
              <tr>
                  <td>Tatyana Fitzpatrick</td>
                  <td>Regional Director</td>
                  <td>London</td>
                  <td>19</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2010/03/17</td>
                  <td>$385,750</td>
              </tr>
             
              <tr>
                  <td>Finn Camacho</td>
                  <td>Support Engineer</td>
                  <td>San Francisco</td>
                  <td>47</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2009/07/07</td>
                  <td>$87,500</td>
              </tr>
              <tr>
                  <td>Serge Baldwin</td>
                  <td>Data Coordinator</td>
                  <td>Singapore</td>
                  <td>64</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2012/04/09</td>
                  <td>$138,575</td>
              </tr>
              <tr>
                  <td>Zenaida Frank</td>
                  <td>Software Engineer</td>
                  <td>New York</td>
                  <td>63</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2010/01/04</td>
                  <td>$125,250</td>
              </tr>
              <tr>
                  <td>Zorita Serrano</td>
                  <td>Software Engineer</td>
                  <td>San Francisco</td>
                  <td>56</td>
                  <td>61</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2012/06/01</td>
                  <td>$115,000</td>
              </tr>
              <tr>
                  <td>Jennifer Acosta</td>
                  <td>Junior Javascript Developer</td>
                  <td>61</td>
                  <td>Edinburgh</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>43</td>
                  <td>2013/02/01</td>
                  <td>$75,650</td>
              </tr>
              <tr>
                  <td>Cara Stevens</td>
                  <td>Sales Assistant</td>
                  <td>61</td>
                  <td>New York</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>46</td>
                  <td>2011/12/06</td>
                  <td>$145,600</td>
              </tr>
              <tr>
                  <td>61</td>
                  <td>Hermione Butler</td>
                  <td>Regional Director</td>
                  <td>London</td>
                  <td>47</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2011/03/21</td>
                  <td>$356,250</td>
              </tr>
              <tr>
                  <td>61</td>
                  <td>Lael Greer</td>
                  <td>Systems Administrator</td>
                  <td>London</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>21</td>
                  <td>2009/02/27</td>
                  <td>$103,500</td>
              </tr>
              <tr>
                  <td>61</td>
                  <td>Jonas Alexander</td>
                  <td>Developer</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>San Francisco</td>
                  <td>30</td>
                  <td>2010/07/14</td>
                  <td>$86,500</td>
              </tr>
              <tr>
                  <td>61</td>
                  <td>Shad Decker</td>
                  <td>Regional Director</td>
                  <td>Edinburgh</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>51</td>
                  <td>2008/11/13</td>
                  <td>$183,000</td>
              </tr>
              <tr>
                  <td>61</td>
                  <td>Michael Bruce</td>
                  <td>Javascript Developer</td>
                  <td>Singapore</td>
                  <td>29</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2011/06/27</td>
                  <td>$183,000</td>
              </tr>
              <tr>
                  <td>61</td>
                  <td>Donna Snider</td>
                  <td>Customer Support</td>
                  <td>New York</td>
                  <td>27</td>
                  <td>3</td>
                  <td>61</td>
                  <td>61</td>
                  <td>61</td>
                  <td>2011/01/25</td>
                  <td>$112,000</td>
              </tr>
          </tbody>
          <tfoot> 
                  <th>No</th>
                  <th>TGL</th>
                  <th>Nama</th>
                  <th>Kategori Produk</th>
                  <th>Nama Produk</th>
                  <th>Ukuran</th>
                  <th>Warna</th>
                  <th>Jumlah Produk</th>
                  <th>Total Harga</th> 
                  <td>Status</td>
                  <th>Aksi (konfirmasi, diambil, batalkan)</th>
          </tfoot>
        </table>
      </div>
    </div>
  </section>

  

<?php $this->load->view('header'); ?>




  <!-- Footer -->
  <footer class="footer">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-4">
          <span class="copyright">Copyright &copy; Your Website 2019</span>
        </div>
        <div class="col-md-4">
          <ul class="list-inline social-buttons">
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
           
          </ul>
        </div>
        <div class="col-md-4">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item">
              <a href="#">Privacy Policy</a>
            </li>
            <li class="list-inline-item">
              <a href="#">Terms of Use</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  

<?php $this->load->view('footer'); ?>

<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

