<!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url(); ?>bahan/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>bahan/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url(); ?>bahan/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact form JavaScript -->
  <script src="<?php echo base_url(); ?>bahan/js/jqBootstrapValidation.js"></script>
  <script src="<?php echo base_url(); ?>bahan/js/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url(); ?>bahan/js/agency.min.js"></script>
  <script src="<?php echo base_url(); ?>bahan/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>bahan/js/dataTables.bootstrap4.min.js"></script>


  <script src="<?php echo base_url() ?>bahan/plugins/validate/jquery.validate.min.js"></script>
  <script src="<?php echo base_url() ?>bahan/plugins/upload/jquery.ui.widget.js"></script>
  <script src="<?php echo base_url() ?>bahan/plugins/upload/jquery.fileupload.js"></script>
  <script src="<?php echo base_url() ?>bahan/plugins/sweetalert/sweetalert2.min.js"></script>

</body>

</html>