<?php $this->load->view('admin/header');?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {judul}
      </h1>
      <p>{subjudul}</p>
      
    </section>

    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
             
            <div class="box-body">
               
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>TANGGAL</th>
                  <th>USERNAME</th>
                  <th>NAMA</th>
                  <th>EMAIL</th>
                  <th>AKSI</th>
                </tr>
                </thead>
                <?php $no=1; foreach ($penjual as $p): ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <th><?php echo date('d-m-Y',strtotime($p['tgl_bergabung'])); ?></th>
                    <td><?php echo $p['username']; ?></td>
                    <td><?php echo $p['nama_toko']; ?></td>
                    <td><?php echo $p['email_toko']; ?></td>
                    <td><button  class="btn btn-danger" onclick="hapus(<?php echo $p['id_toko'] ?>)"><i class="fa fa-trash-o"></i> Hapus</button></td>
                  </tr>
                <?php endforeach ?>
                <tbody>
    
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<?php $this->load->view('admin/footer');?>
<script type="text/javascript">

$(function () {
  $('#example1').DataTable();
})

function hapus(id)
{
  Swal.fire({
    title: 'Apakah anda yakin akan menghapus data ini ?',
    text: "Data yang sudah di hapus tidak dapat dikembalikan lagi",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak'
  }).then((result) => {
    if (result.value) {
      $.ajax({
          url : "<?php echo site_url('admin_kelola_penjual/ajax_hapus')?>/"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data berhasil dihapus',
                showConfirmButton: false,
                timer: 2000
              })
              location.reload();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              Swal.fire({
                position: 'center',
                type: 'error',
                title: 'Oops...',
                text: 'Error menghapus data',
                timer: 5000
              })
          }
      });
    }
  })
}

</script>


  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.13
    </div>
    <strong>Copyright LOOKALL &copy; 2019 </strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
  
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('admin/footer');?>
