<?php $this->load->view('admin/header');?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        KATEGORI LOOKALL
      </h1> 
    </section>

    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
             
            <div class="box-body">
              <button  class="btn btn-success" onclick="tambah()">
                <i class="icon-plus3"></i> Tambah Kategori
              </button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="10%">NO</th>
                  <th width="70%">Kategori</th> 
                  <th width="20%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($kategori as $kat): ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $kat['nama_ktg'] ?></td>
                    <td>
                      <button  class="btn btn-warning" onclick="edit(<?php echo $kat['id_ktg'] ?>)"><i class="fa fa-cog"></i> Edit</button>
                      <button  class="btn btn-danger" onclick="hapus(<?php echo $kat['id_ktg'] ?>)"><i class="fa fa-trash-o"></i> Hapus</button>
                    </td>           
                  </tr>
                  <?php endforeach ?>
                </tbody>
    
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Kategori Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form">
                    <input id="id_ktg" name="id_ktg" class="form-control" type="hidden">
                    <div class="form-body">
                        <div class="form-group">
                          <label for="username">Nama Kategori</label>
                          <input id="nama_ktg" name="nama_ktg" placeholder="Mohon isikan nama kategori" class="form-control" type="text">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php $this->load->view('admin/footer');?>
<script type="text/javascript">
$(function () {
  $('#example1').DataTable();
})
var save_method; //for save method string
var table;

function tambah()
{
    save_method = 'tambah';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Kategori'); // Set Title to Bootstrap modal title
}

$("#btnSave").click(function() {
    $("#form").submit();
});

$("#form").validate({
    rules: {
        nama_ktg: "required"
    },
    messages: {
        nama_ktg: "Mohon isikan nama kategori",
    },
    submitHandler: function() {
      simpan();
    }
});

function edit(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('admin_kelola_kategori/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#nama_ktg').val(data.nama_ktg);
            $('#id_ktg').val(data.id_ktg);
            $('.modal-title').text('Edit Kategori');
            $('#modal_form').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}


function simpan()
{
    $('#btnSave').text('menyimpan...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'tambah') {
        url = "<?php echo site_url('admin_kelola_kategori/ajax_simpan')?>";
    } else {
        url = "<?php echo site_url('admin_kelola_kategori/ajax_update')?>";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data Berhasil disimpan',
                showConfirmButton: false,
                timer: 2000
              })
              $('#modal_form').modal('hide');
              setTimeout(function() {
                location.reload();
              }, 2000);
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          Swal.fire({
            position: 'center',
            type: 'error',
            title: 'Oops...',
            text: 'Error menambahkan / mengupdate data',
            timer: 5000
          })
          $('#btnSave').text('save'); //change button text
          $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function hapus(id)
{
  Swal.fire({
    title: 'Apakah anda yakin akan menghapus data ini ?',
    text: "Data yang sudah di hapus tidak dapat dikembalikan lagi",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak'
  }).then((result) => {
    if (result.value) {
      $.ajax({
          url : "<?php echo site_url('admin_kelola_kategori/ajax_hapus')?>/"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data berhasil dihapus',
                showConfirmButton: false,
                timer: 2000
              })
              location.reload();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              Swal.fire({
                position: 'center',
                type: 'error',
                title: 'Oops...',
                text: 'Error menghapus data',
                timer: 5000
              })
          }
      });
    }
  })
}

</script>