 <?php $this->load->view('admin/header');?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        GRAFIK PENJUALAN LOOKALL
      </h1>
      <ol class="breadcrumb">
      
      </ol>
    </section>
<!-- /.box-header GRAFIK-->
           
				<div class="col-md-12">
		          <div class="box">
		            <div class="box-header with-border">
		             
 
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		              <div class="row">
		                <div class="col-md-8">
		                  <p class="text-center">
		                    <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
		                  </p>

		                  <div class="chart">
		                    <!-- Sales Chart Canvas -->
		                    <canvas id="salesChart" style="height: 180px; width: 645px;" height="180" width="645"></canvas>
		                  </div>
		                  <!-- /.chart-responsive -->
		                </div>
		                <!-- /.col -->
		                <div class="col-md-4">
		                  <p class="text-center">
		                    <strong>Goal Completion</strong>
		                  </p>

		                  <div class="progress-group">
		                    <span class="progress-text">Add Products to Cart</span>
		                    <span class="progress-number"><b>160</b>/200</span>

		                    <div class="progress sm">
		                      <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
		                    </div>
		                  </div>
		                  <!-- /.progress-group -->
		                  <div class="progress-group">
		                    <span class="progress-text">Complete Purchase</span>
		                    <span class="progress-number"><b>310</b>/400</span>

		                    <div class="progress sm">
		                      <div class="progress-bar progress-bar-red" style="width: 80%"></div>
		                    </div>
		                  </div>
		                  <!-- /.progress-group -->
		                  <div class="progress-group">
		                    <span class="progress-text">Visit Premium Page</span>
		                    <span class="progress-number"><b>480</b>/800</span>

		                    <div class="progress sm">
		                      <div class="progress-bar progress-bar-green" style="width: 80%"></div>
		                    </div>
		                  </div>
		                  <!-- /.progress-group -->
		                  <div class="progress-group">
		                    <span class="progress-text">Send Inquiries</span>
		                    <span class="progress-number"><b>250</b>/500</span>

		                    <div class="progress sm">
		                      <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
		                    </div>
		                  </div>
		                  <!-- /.progress-group -->
		                </div>
		                <!-- /.col -->
		              </div>
		              <!-- /.row -->
		            </div>
		            <!-- ./box-body -->
		            <div class="box-footer">
		              <div class="row">
		                <div class="col-sm-3 col-xs-6">
		                  <div class="description-block border-right">
		                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
		                    <h5 class="description-header">$35,210.43</h5>
		                    <span class="description-text">TOTAL REVENUE</span>
		                  </div>
		                  <!-- /.description-block -->
		                </div>
		                <!-- /.col -->
		                <div class="col-sm-3 col-xs-6">
		                  <div class="description-block border-right">
		                    <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
		                    <h5 class="description-header">$10,390.90</h5>
		                    <span class="description-text">TOTAL COST</span>
		                  </div>
		                  <!-- /.description-block -->
		                </div>
		                <!-- /.col -->
		                <div class="col-sm-3 col-xs-6">
		                  <div class="description-block border-right">
		                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
		                    <h5 class="description-header">$24,813.53</h5>
		                    <span class="description-text">TOTAL PROFIT</span>
		                  </div>
		                  <!-- /.description-block -->
		                </div>
		                <!-- /.col -->
		                <div class="col-sm-3 col-xs-6">
		                  <div class="description-block">
		                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
		                    <h5 class="description-header">1200</h5>
		                    <span class="description-text">GOAL COMPLETIONS</span>
		                  </div>
		                  <!-- /.description-block -->
		                </div>
		              </div>
		              <!-- /.row -->
		            </div>
		            <!-- /.box-footer -->
		          </div>
		          <!-- /.box -->
		        </div>

 

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

       
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.13
    </div>
    <strong>Copyright LOOKALL &copy; 2019 </strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
  
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('admin/footer');?>
