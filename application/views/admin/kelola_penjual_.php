<?php $this->load->view('admin/header');?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        LAPORAN PENJUAL LOOKALL
      </h1>
      
    </section>

    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
           
            <div class="box-body">
            
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>NIM</th>
                  <th>NAMA</th>
                  <th>TEMPAT & TGL LAHIR</th>
                  <th>ALAMAT</th>
                  <th>ACTION (HAPUS data)</th>
                </tr>
                </thead> 
                  <tr>
                    <td>1</td>
                    <th>12 agust 2019</th>
                    <td>jum</td>
                    <td>situl</td>
                    <td>laki laki</td>
                    <td>2</td>
                  </tr>

                  <tr>
                    <td>2</td>
                    <th>12 sep 2019</th>
                    <td>qwerty</td>
                    <td>cubi</td>
                    <td>perempuan</td>
                    <td>2</td>
                  </tr>
                		
                  <tr>
                    <td>1</td>
                    <th>12 agust 2019</th>
                    <td>jum</td>
                    <td>situl</td>
                    <td>laki laki</td>
                    <td>2</td>
                  </tr>

                  <tr>
                    <td>2</td>
                    <th>12 sep 2019</th>
                    <td>qwerty</td>
                    <td>cubi</td>
                    <td>perempuan</td>
                    <td>2</td>
                  </tr>
                  <tr>
                    <td>1</td>
                    <th>12 agust 2019</th>
                    <td>jum</td>
                    <td>situl</td>
                    <td>laki laki</td>
                    <td>2</td>
                  </tr>

                  <tr>
                    <td>2</td>
                    <th>12 sep 2019</th>
                    <td>qwerty</td>
                    <td>cubi</td>
                    <td>perempuan</td>
                    <td>2</td>
                  </tr>
                  <tr>
                    <td>1</td>
                    <th>12 agust 2019</th>
                    <td>jum</td>
                    <td>situl</td>
                    <td>laki laki</td>
                    <td>2</td>
                  </tr>

                  <tr>
                    <td>2</td>
                    <th>12 sep 2019</th>
                    <td>qwerty</td>
                    <td>cubi</td>
                    <td>perempuan</td>
                    <td>2</td>
                  </tr>
                <tbody>
    
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">{title} Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form">
                    <input type="hidden" id="ID_USER" name="ID_USER"/> 
                    <div class="form-body">
                        <div class="form-group">
                          <label for="username">NIM</label>
                          <input id="NIM" name="NIM" placeholder="Username" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                          <label for="username">PASSWORD</label>
                          <input id="PASSWORD" name="PASSWORD" placeholder="Password" class="form-control" type="password">
                        </div>
                        <div class="form-group">
                          <label for="username">NAMA</label>
                          <input id="NAMA" name="NAMA" placeholder="Nama Lengkap Mahasiswa" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                          <label for="username">TEMPAT LAHIR</label>
                          <input id="TEMPAT_LAHIR" name="TEMPAT_LAHIR" placeholder="Tempat Lahir" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                          <label for="date">TANGGAL LAHIR</label>
                          <input id="TANGGAL_LAHIR" name="TANGGAL_LAHIR" placeholder="Tanggal Lahir" class="form-control" value="<?=date('d-m-Y')?>" type="date">
                        </div>
                        <div class="form-group">
                          <label for="username">ALAMAT</label>
                          <textarea rows="3" id="ALAMAT" name="ALAMAT" placeholder="Alamat Mahasiswa" class="form-control" type="text"></textarea>
                        </div>
                        <div class="form-group">
                          <label for="email">EMAIL</label>
                          <input id="EMAIL" name="EMAIL" placeholder="Email" class="form-control" type="email">
                        </div>
                        <div class="form-group">
                          <label for="email">NO HP</label>
                          <input id="NO_HP" name="NO_HP" placeholder="Nomor Handphone" class="form-control" type="number">
                        </div>
                        <div class="form-group">
                          <label for="group">FAKULTAS</label>
                          <select id="ID_FAK" name="ID_FAK" class="form-control">
                            <option value="">Pilih Fakultas</option>
                            {fakultas}
                            <option value="{ID_FAK}">{NAMA_FAK}</option>
                            {/fakultas}
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="group">PRODI</label>
                          <select id="ID_PRODI" name="ID_PRODI" class="form-control">
                            <option value="">Pilih Prodi</option>
                            {prodi}
                            <option value="{ID_PRODI}">{NAMA_PRODI}</option>
                            {/prodi}
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="group">PERAN</label>
                          <select id="ID_PERAN" name="ID_PERAN" class="form-control">
                            <option value="">Pilih Peran</option>
                            {peran}
                            <option value="{ID_PERAN}">{NAMA}</option>
                            {/peran}
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="foto_file">FOTO</label><br />
                          <input id="foto_file" name="file" type="file" multiple>
                          <div id="foto_file-preview"></div>
                          <input type="hidden" id="FOTO" name="FOTO">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php $this->load->view('admin/footer');?>
<script type="text/javascript">
$(function () {
  $('#example1').DataTable();
})
var save_method; //for save method string
var table;

$("#btnSave").click(function() {
    $("#form").submit();
});

$(function () {
    'use strict';
    var url_foto = "<?php echo site_url('user/upload_foto')?>";
    $('#foto_file').fileupload({
        url: url_foto,
        dataType: 'json',
        done: function (e, data) {
            window.foto = data.result.file;
            var imgPrev = '<img src="'+window.baseUrl+'/'+window.foto+'" style="margin-top: 20px;" width="300" />';
            $("#foto_file-preview").html(imgPrev);
            $("#FOTO").val(window.foto);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

if (save_method == 'add') {
  $("#form").validate({
      rules: {
          NIM: "required",
          PASSWORD: "required",
          NAMA: "required",
          TEMPAT_LAHIR: "required",
          TANGGAL_LAHIR: "required",
          ALAMAT: "required",
          EMAIL: "required",
          NO_HP: "required",
          ID_FAK: "required",
          ID_PRODI: "required"
      },
      messages: {
          NIM: "Mohon isikan NIM",
          PASSWORD: "Mohon isikan Password",
          NAMA: "Mohon isikan Nama Lengkap",
          TEMPAT_LAHIR: "Mohon isikan Tempat Lahir",
          TANGGAL_LAHIR: "Mohon isikan Tanggal Lahir",
          ALAMAT: "Mohon isikan Alamat",
          EMAIL: "Mohon isikan Email",
          NO_HP: "Mohon isikan No HP",
          ID_FAK: "Mohon isikan Fakultas",
          ID_PRODI: "Mohon isikan Prodi",
      },
      submitHandler: function() {
        save();
      }
  });
} else {
  $("#form").validate({
      rules: {
          NIM: "required",
          NAMA: "required",
          TEMPAT_LAHIR: "required",
          TANGGAL_LAHIR: "required",
          ALAMAT: "required",
          EMAIL: "required",
          NO_HP: "required",
          ID_FAK: "required",
          ID_PRODI: "required"
      },
      messages: {
          NIM: "Mohon isikan NIM",
          NAMA: "Mohon isikan Nama Lengkap",
          TEMPAT_LAHIR: "Mohon isikan Tempat Lahir",
          TANGGAL_LAHIR: "Mohon isikan Tanggal Lahir",
          ALAMAT: "Mohon isikan Alamat",
          EMAIL: "Mohon isikan Email",
          NO_HP: "Mohon isikan No HP",
          ID_FAK: "Mohon isikan Fakultas",
          ID_PRODI: "Mohon isikan Prodi",
      },
      submitHandler: function() {
        save();
      }
  });
}

function add()
{
    $('#ID_USER').val(0);
    save_method = 'add';
    $('#NIM').attr('disabled',false);
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add User'); // Set Title to Bootstrap modal title
}

function edit(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('user/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#ID_USER').val(data.ID_USER);
            $('[name="ID_PERAN"]').val(data.ID_PERAN);
            $('[name="ID_PRODI"]').val(data.ID_PRODI);
            $('[name="ID_FAK"]').val(data.ID_FAK);
            $('[name="NIM"]').val(data.NIM);
            $('#NIM').attr('disabled',true);
            $('[name="NAMA"]').val(data.NAMA);
            $('[name="TEMPAT_LAHIR"]').val(data.TEMPAT_LAHIR);
            $('[name="TANGGAL_LAHIR"]').val(data.TANGGAL_LAHIR);
            $('[name="ALAMAT"]').val(data.ALAMAT);
            $('[name="EMAIL"]').val(data.EMAIL);
            // $('[name="PASSWORD"]').val(data.PASSWORD);
            $('[name="NO_HP"]').val(data.NO_HP);
            window.foto = data.FOTO;
            var imgPrev = '<img src="'+window.baseUrl+'/'+window.foto+'" style="margin-top: 20px;" width="300" />';
            $("#foto_file-preview").html(imgPrev);
            $("#FOTO").val(window.foto);
            $('.modal-title').text('Edit User');
            $('#modal_form').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}


function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
        url = "<?php echo site_url('user/ajax_add')?>";
    } else {
        url = "<?php echo site_url('user/ajax_update')?>";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data Berhasil disimpan',
                showConfirmButton: false,
                timer: 2000
              })
              $('#modal_form').modal('hide');
              setTimeout(function() {
                location.reload();
              }, 2000);
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          Swal.fire({
            position: 'center',
            type: 'error',
            title: 'Oops...',
            text: 'Error menambahkan / mengupdate data',
            timer: 5000
          })
          $('#btnSave').text('save'); //change button text
          $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function hapus(id)
{
  Swal.fire({
    title: 'Apakah anda yakin akan menghapus data ini ?',
    text: "Data yang sudah di hapus tidak dapat dikembalikan lagi",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak'
  }).then((result) => {
    if (result.value) {
      $.ajax({
          url : "<?php echo site_url('user/ajax_delete')?>/"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data berhasil dihapus',
                showConfirmButton: false,
                timer: 2000
              })
              location.reload();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              Swal.fire({
                position: 'center',
                type: 'error',
                title: 'Oops...',
                text: 'Error menghapus data',
                timer: 5000
              })
          }
      });
    }
  })
}

</script>