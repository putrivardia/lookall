<?php $this->load->view('header'); 
function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
  return $hasil_rupiah;
 
}
?>

  <!-- Keranjang toko Grid -->
  <section class="bg-light page-section" id="portfolio">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Keranjang Saya</h2>
       </div>
      </div>
      <div class="row">
        <table id="example" class="table table-striped table-bordered">
          <thead>
              <tr>
                  <th>No</th>
                  <th>Foto</th>
                  <th>Barang</th>
                  <th>Jumlah Pesanan</th>
                  <th>Harga</th>
                  <th>Status</th>
                  <th>Aksi</th>
              </tr>
          </thead>
          <tbody>
            <?php $no = 1; foreach ($pesanan as $list): ?>
              <tr>
                  <td><?php echo $no++; ?></td>
                  <td><img class="lazy" src="<?php echo base_url(); ?><?php echo $list['foto_brg'] ?>" style="display: block;width: 100px; height: 100px;" ></td>
                  <td>
                    <table>
                      <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><?php echo $list['nama_brg'] ?></td>
                      </tr>
                      <tr>
                        <td>Warna</td>
                        <td>:</td>
                        <td><?php echo $list['warna_brg'] ?></td>
                      </tr>
                      <tr>
                        <td>SKU</td>
                        <td>:</td>
                        <td><?php echo $list['sku_brg'] ?></td>
                      </tr>
                      <tr>
                        <td>Harga</td>
                        <td>:</td>
                        <td><?php echo rupiah($list['harga_brg']) ?></td>
                      </tr>
                    </table>
                  </td>
                  <td><?php echo $list['quantity'] ?></td> 
                  <td><?php $tot = $list['harga_brg']*$list['quantity']; echo rupiah($tot);?></td>
                  <td><div class="text-uppercase"><?php echo $list['status_pemesanan'] ?></div></td> 
                  <td>
                  	<?php if ($list['status_pemesanan']=="menunggu konfirmasi"): ?>
                  	<button type="button" class="btn btn-primary btn-l text-uppercase" onclick="batalkanpesanan(<?php echo $list['id_pemesanan'] ?>,<?php echo $list['id_brg'] ?>,<?php echo $list['quantity'] ?>)">Batalkan</button>	
                  	<?php endif ?>
                  	<?php if ($list['status_pemesanan']=="dibatalkan" || $list['status_pemesanan']=="sudah diambil"): ?>
                  	<button type="button" class="btn btn-dangger btn-l text-uppercase" onclick="hapusriwayat(<?php echo $list['id_detailpemesanan'] ?>)" style="background-color: red;color: white;margin-bottom: 5px;">Hapus</button>
                  	<?php endif ?>
                  </td>
              </tr>
            <?php endforeach ?>
          </tbody>
          <tfoot>
               
          </tfoot>
        </table>
        
        </div>
      </div>
    </div>
  </section>  

<?php $this->load->view('footer'); ?>

<script type="text/javascript">

$(document).ready(function() {
    $('#example').DataTable();
} );

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix){
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split       = number_string.split(','),
  sisa        = split[0].length % 3,
  rupiah        = split[0].substr(0, sisa),
  ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
 
  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }
 
  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function hapusriwayat(id)
{
  Swal.fire({
    title: 'Apakah anda yakin akan menghapus data ini ?',
    text: "Data yang sudah di hapus tidak dapat dikembalikan lagi",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak'
  }).then((result) => {
    if (result.value) {
      $.ajax({
          url : "<?php echo site_url('riwayat/ajax_hapus')?>/"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data berhasil dihapus',
                showConfirmButton: false,
                timer: 2000
              })
              setTimeout(function() {
	            location.reload();
	          }, 2000);
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              Swal.fire({
                position: 'center',
                type: 'error',
                title: 'Oops...',
                text: 'Error menghapus data',
                timer: 5000
              })
          }
      });
    }
  })
}

function batalkanpesanan(id,brg,qty)
{
  Swal.fire({
    title: 'Apakah anda yakin akan membatalkan pesanan ini ?',
    text: "Data yang sudah di batalkan tidak dapat dikembalikan lagi",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak'
  }).then((result) => {
    if (result.value) {
      $.ajax({
          url : "<?php echo site_url('riwayat/ajax_batal')?>",
          type: "POST",
          data: {
            id: id
            ,brg: brg
            ,qty: qty
          },
          dataType: "JSON",
          success: function(data)
          {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data berhasil dihapus',
                showConfirmButton: false,
                timer: 2000
              })
              setTimeout(function() {
                  location.reload();
              }, 2000);
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              Swal.fire({
                position: 'center',
                type: 'error',
                title: 'Oops...',
                text: 'Error menghapus data',
                timer: 5000
              })
          }
      });
    }
  })
}
</script>

