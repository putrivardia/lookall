<?php $this->load->view('header'); ?>



  <!-- daftar produk toko -->
  <section style="margin-top: 25px; " class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">DAFTARKAN PRODUK</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form id="contactForm" name="sentMessage" novalidate="novalidate">
            <div class="row justify-content-center" >
              <div class="col-md-6">
                 
                  <div class="form-group">
                  <label style="color: white;">Nama Produk :</label>
                  <input class="form-control" id="namaproduk" type="text" placeholder="Nama Produk *" required="required" data-validation-required-message=" Masukan Nama Produk">
                  <p class="help-block text-danger"></p>
                </div>
                  <div class="form-group">
                  <label style="color: white;">Harga Produk :</label>
                  <input class="form-control" id="hargaproduk" type="text" placeholder="Harga Produk *" required="required" data-validation-required-message="Harga Produk">
                  <p class="help-block text-danger"></p>
                </div>
              
                     <div class="form-group">
                  <label style="color: white;">Ukuran Produk :</label>
                  <textarea class="form-control" id="ukuranproduk" placeholder="Ukuran Produk *" required="required" data-validation-required-message="Ukuran Produk."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label style="color: white;">Warna Produk :</label>
                  <textarea class="form-control" id="warnaproduk" placeholder="Warna Produk *" required="required" data-validation-required-message="Warna Produk."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
                  <div class="form-group">
                  <label style="color: white;">Berat Produk :</label>
                  <textarea class="form-control" id="beratproduk" placeholder="Berat Produk *" required="required" data-validation-required-message="Berat Produk."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
                  <div class="form-group">
                  <label style="color: white;">Stok Produk :</label>
                  <textarea class="form-control" id="stokproduk" placeholder="Stok Produk *" required="required" data-validation-required-message="Stok Produk."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
                   <div class="form-group">
                  <label style="color: white;">SKU Produk :</label>
                  <textarea class="form-control" id="skuproduk" placeholder="SKU Produk *" required="required" data-validation-required-message="SKU Produk."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div>
           <!--    <div class="col-md-6">
                <div class="form-group">
                  <textarea class="form-control" id="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div> -->
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center">
                <div id="success"></div>
                <button  href="<?php echo base_url(); ?>profil_toko" id="sendMessageButton" class="btn btn-primary btn-l text-uppercase" type="submit">Daftarkan</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer class="footer">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-4">
          <span class="copyright">Copyright &copy; Your Website 2019</span>
        </div>
        <div class="col-md-4">
          <ul class="list-inline social-buttons">
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-linkedin-in"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="col-md-4">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item">
              <a href="#">Privacy Policy</a>
            </li>
            <li class="list-inline-item">
              <a href="#">Terms of Use</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  

<?php $this->load->view('footer'); ?>
