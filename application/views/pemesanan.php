<?php $this->load->view('header'); 
function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
  return $hasil_rupiah;
 
}
?>

  <!-- Kategori & menu toko Grid -->
  <div class="row" style="margin-top:120px">
    <div class="col-lg-12 text-center">
      <h2 class="section-heading text-uppercase">Daftar Pemesanan Barang</h2>
    </div>
    <div class="col-lg-12 text-center">
      <div class="box-body">     
      </div>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>NO</th>
            <th>NAMA BARANG</th>
            <th>FOTO</th>
            <th>WARNA</th>
            <th>UKURAN</th>
            <th>JUMLAH</th>
            <th>TOTAL</th>
            <th>STATUS</th>
            <th>AKSI</th>
          </tr>
        </thead>
        <tbody>
      </table>
    </div>
  </div>
  

<?php $this->load->view('footer'); ?>

<script type="text/javascript">

save_method = 'tambah';
window.baseUrl = '<?php echo base_url(); ?>';

$(function () {
  $('#example1').DataTable();
})

</script>

