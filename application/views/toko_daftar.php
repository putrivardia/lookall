<?php $this->load->view('header'); ?>



  <!-- daftar toko -->
  <section style="margin-top: 25px; " class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">DAFTAR PENJUAL</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form id="form" name="form" novalidate="novalidate">
            <div class="row justify-content-center" >
              <div class="col-md-6">
                <div class="form-group">
                  <label style="color: white;">Username :</label>
                  <input class="form-control" id="username" name="username" type="text" placeholder="Isikan Username *" required="required" data-validation-required-message="Masukan username anda">
                  <p class="help-block text-danger"></p>
                </div>

                <div class="form-group">
                  <label style="color: white;">Kata Sandi :</label>
                  <input class="form-control" id="password" type="password" name="password" placeholder="Isikan Password*" required="required" data-validation-required-message=" Masukan password anda">
                  <p class="help-block text-danger"></p>
                </div>

                <div class="form-group">
                  <label style="color: white;">Nama Toko:</label>
                  <input class="form-control" id="nama_toko" name="nama_toko" type="text" placeholder="Isikan Nama Toko *" required="required" data-validation-required-message=" Masukan Nama Toko">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label style="color: white;">Email Toko :</label>
                  <input class="form-control" id="email_toko" name="email_toko" type="text" placeholder="Isikan Email *" required="required" data-validation-required-message="Masukan Email">
                  <p class="help-block text-danger"></p>
                </div>
              
                <div class="form-group">
                  <label style="color: white;">Alamat Toko:</label>
                  <textarea class="form-control" id="alamat_toko" name="alamat_toko" placeholder="Isikan Alamat Toko *" required="required" data-validation-required-message="Masukan Alamat."></textarea>
                  <p class="help-block text-danger"></p>
                </div>

                  <label style="color: white;">Titik Lokasi Toko :</label><br>
                <div class="input-group input-group">
                  <input type="text" class="form-control" id="koordinat_toko" name="koordinat_toko" placeholder="Isikan Titik Lokasi *">
                  <span class="input-group-btn">
                    <button type="button" onclick="loadthemappicker()" class="btn btn-info btn-flat">Pilih Lokasi</button>
                  </span>
                </div>
              
             
              </div>
              <div class="clearfix">&nbsp;</div>
              <div class="col-lg-12 text-center">
                <br>
                <button id="btnSave" class="btn btn-primary btn-l text-uppercase" type="button">Daftar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer class="footer">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-4">
          <span class="copyright">Copyright &copy; Your Website 2019</span>
        </div>
        <div class="col-md-4">
          <ul class="list-inline social-buttons">
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-linkedin-in"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="col-md-4">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item">
              <a href="#">Privacy Policy</a>
            </li>
            <li class="list-inline-item">
              <a href="#">Terms of Use</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <!-- Form Edit konsumen-->
  <div class="modal fade" id="modallokasi">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h2 class="text-uppercase">Form Edit Akunku</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="container">
          <div class="modal-body form">
              <div class="alert alert-warning">Peta dibawah tidak tampil ? coba klik <button class="btn btn-warning btn-sm" onclick="loadthemappicker()">Load Peta Ulang</button></div>
              Koordinat : <p id="koordinat"></p>
              <div id="map" style="height: 300px!important;"></div>

              <div class="modal-footer">
                <button type="button" class="btn btn-success btn-l" data-dismiss="modal">
                    <i class="fa fa-check"></i> Pilih Lokasi
                </button>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  

<?php $this->load->view('footer'); ?>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyDya4920f_VwXfZcxkSmtsxDEgYT_pI-b8&sensor=false"></script>

<script type="text/javascript">
function loadthemappicker()
{
    $('#modallokasi').modal('show');
    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 12,
    center: new google.maps.LatLng(-7.781921,110.364678),
     mapTypeId: google.maps.MapTypeId.ROADMAP
      });
    //posisi awal marker   
    var latLng = new google.maps.LatLng(-7.781921,110.364678);
     
    /* buat marker yang bisa di drag lalu 
      panggil fungsi updateMarkerPosition(latLng)
     dan letakan posisi terakhir di id=latitude dan id=longitude
     */
    var marker = new google.maps.Marker({
        position : latLng,
        title : 'lokasi',
        map : map,
        draggable : true
      });
       
    updateMarkerPosition(latLng);
    google.maps.event.addListener(marker, 'drag', function() {
     // ketika marker di drag, otomatis nilai latitude dan longitude
     //menyesuaikan dengan posisi marker 
        updateMarkerPosition(marker.getPosition());
    });
}

function updateMarkerPosition(latLng) {
  $("#koordinat").html([latLng.lat()]+','+[latLng.lng()]);
  $("#koordinat_toko").val([latLng.lat()]+','+[latLng.lng()]);
}

$("#btnSave").click(function() {
    $("#form").submit();
});

$.validator.addMethod("valueNotEquals",function(value, element, arg) {
  return arg !== value;
}, "Value must not equal arg.");

$("#form").validate({
    rules: {
        username: "required",
        password: "required",
        nama_toko: "required",
        email_toko: "required",
        alamat_toko: "required",
        koordinat_toko: "required"
    },
    messages: {
        username: "Mohon isikan username anda",
        password: "Mohon isikan password anda",
        nama_toko: "Mohon isikan nama toko anda",
        email_toko: "Mohon isikan email anda",
        alamat_toko: "Mohon isikan alamat toko anda",
        koordinat_toko: "Mohon pilih lokasi untuk mengisikan koordinat toko anda"
    },
    submitHandler: function() {
      simpan();
    }
});

function simpan()
{
    $('#btnSave').text('menyimpan...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url = "<?php echo site_url('toko_daftar/ajax_tambah')?>";
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data Berhasil disimpan',
                showConfirmButton: false,
                timer: 2000
              })
              setTimeout(function() {
                location.reload();
              }, 2000);
            }

            $('#btnSave').text('Daftar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          Swal.fire({
            position: 'center',
            type: 'error',
            title: 'Oops...',
            text: 'Error menambahkan data',
            timer: 5000
          })
          $('#btnSave').text('Daftar'); //change button text
          $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}
</script>