<?php $this->load->view('header'); ?>


  <!-- About -->
  <section class="page-section" id="about">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">About</h2> 
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <ul class="timeline">
            <li>
              <div class="timeline-image">
                <img class="rounded-circle img-fluid" src="img/about/1.jpg" alt="">
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h4>2019</h4>
                  <h4 class="subheading">Kita Memulai</h4>
                </div>
                <div class="timeline-body">
                  <p class="text-muted">blablablabalbalbala</p>
                </div>
              </div>
            </li>
            
            
            </li>
            
            <li class="timeline-inverted">
              <div class="timeline-image">
                <h4>Terima 
                  <br>Kasih
                  <br></h4>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>


<?php $this->load->view('footer'); ?>
