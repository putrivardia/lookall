<?php $this->load->view('header'); ?>



  <!-- daftar -->
  <section style="margin-top: 25px; " class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">DAFTAR PEMBELI</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form id="form" novalidate="novalidate">
            <div class="row justify-content-center">
              <div class="col-md-6">
                 <div class="form-group">
                  <label style="color: white;">Username :</label>
                  <input class="form-control" id="username" name="username" type="text" placeholder="Silahkan Isikan Username *" required="required" data-validation-required-message="Username">
                  <p class="help-block text-danger"></p>
                </div> 
                <div class="form-group">
                  <label style="color: white;">Kata Sandi :</label>
                  <input class="form-control" id="password" name="password" type="password" placeholder="Silahkan Isikan Kata Sandi*" required="required" data-validation-required-message="Silahkan Isikan Kata Sandi.">
                  <p class="help-block text-danger"></p>
                </div>
                 
                  <div class="form-group">
                  <label style="color: white;">Nama Lengkap:</label>
                  <input class="form-control" id="nama_pembeli" name="nama_pembeli" type="text" placeholder="Silahkan Isikan Nama Lengkap *" required="required" data-validation-required-message="Silahkan Isikan Nama Lengkap">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label style="color: white;">Nomor HP :</label>
                  <input class="form-control" id="nohp_pembeli" name="nohp_pembeli" type="text" placeholder="Silahkan Isikan Nomor HP *" required="required" data-validation-required-message="Silahkan Isikan Nomor HP">
                  <p class="help-block text-danger"></p>
                </div>

                <div class="form-group">
                  <label style="color: white;">Email:</label>
                  <input class="form-control" id="email_pembeli" name="email_pembeli" type="email" placeholder="Silahkan Isikan Email *" required="required" data-validation-required-message="Silahkan Isikan Email">
                  <p class="help-block text-danger"></p>
                </div>
               <div class="form-group">
                  <label style="color: white;">Jenis Kelamin:</label>
                  <select class="form-control" id="kelamin_pembeli" name="kelamin_pembeli" required="required" data-validation-required-message="Pilih Jenis Kelamin" >
                    <option value="0">Pilih Jenis Kelamin</option>
                    <option value="laki-laki">Laki-laki</option>
                    <option value="perempuan">Perempuan</option>
                  </select>
                  <p class="help-block text-danger"></p>
                </div>

               
             
              </div>
           <!--    <div class="col-md-6">
                <div class="form-group">
                  <textarea class="form-control" id="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div> -->
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center">
                <button id="btnSave" class="btn btn-primary btn-l text-uppercase" type="button">Daftar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer class="footer">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-4">
          <span class="copyright">Copyright &copy; Your Website 2019</span>
        </div>
        <div class="col-md-4">
          <ul class="list-inline social-buttons">
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-linkedin-in"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="col-md-4">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item">
              <a href="#">Privacy Policy</a>
            </li>
            <li class="list-inline-item">
              <a href="#">Terms of Use</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  

<?php $this->load->view('footer'); ?>

<script type="text/javascript">

$("#btnSave").click(function() {
    $("#form").submit();
});

$.validator.addMethod("valueNotEquals",function(value, element, arg) {
  return arg !== value;
}, "Value must not equal arg.");

$("#form").validate({
    rules: {
        username: "required",
        password: "required",
        nama_pembeli: "required",
        nohp_pembeli: "required",
        email_pembeli: "required",
        kelamin_pembeli: {valueNotEquals: "0"}
    },
    messages: {
        username: "Mohon isikan username anda",
        password: "Mohon isikan password anda",
        nama_pembeli: "Mohon isikan nama lengkap anda",
        nohp_pembeli: "Mohon isikan nomor HP anda",
        email_pembeli: "Mohon isikan email anda",
        kelamin_pembeli: {valueNotEquals: "Silahkan pilih jenis kelamin"}
    },
    submitHandler: function() {
      simpan();
    }
});

function simpan()
{
    $('#btnSave').text('menyimpan...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url = "<?php echo site_url('konsumen_daftar/ajax_tambah')?>";
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data Berhasil disimpan',
                showConfirmButton: false,
                timer: 2000
              })
              setTimeout(function() {
                location.reload();
              }, 2000);
            }

            $('#btnSave').text('Daftar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          Swal.fire({
            position: 'center',
            type: 'error',
            title: 'Oops...',
            text: 'Error menambahkan data',
            timer: 5000
          })
          $('#btnSave').text('Daftar'); //change button text
          $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

</script>