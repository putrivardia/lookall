<?php $this->load->view('header'); 
function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
  return $hasil_rupiah;
 
}
?>

  <!-- Kategori & menu toko Grid -->
  <div class="row" style="margin-top:100px">
    <div class="col-lg-12 text-center">
      <h2 class="section-heading text-uppercase">Produk Kami</h2>
    </div>
    <div class="col-lg-12 text-center">
      <div class="box-body">
        <button type="button" class="btn btn-primary btn-l text-uppercase" onclick="tambah()">Tambah Barang</button>       
      </div>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>NO</th>
            <th>KATEGORI</th>
            <th>NAMA BARANG</th>
            <th>FOTO</th>
            <th>DESKRIPSI</th>
            <th>WARNA</th>
            <th>UKURAN</th>
            <th>STOK</th>
            <th>SKU</th>
            <th>HARGA</th>
            <th>AKSI</th>
          </tr>
        </thead>
        <?php $no=1; foreach ($barang as $brg): ?>
          <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $brg['nama_ktg']; ?></td>
            <td><?php echo $brg['nama_brg']; ?></td>
            <td><img class="lazy" alt="" src="<?php echo base_url(); ?><?php echo $brg['foto_brg'] ?>" style="display: block;width: 100px; height: 100px;border-radius:50%;" ></td>
            <td><?php echo $brg['deskripsi_brg']; ?></td>
            <td><?php echo $brg['warna_brg']; ?></td>
            <td><?php echo $brg['ukuran_brg']; ?></td>
            <td><?php echo $brg['stok_brg']; ?></td>
            <td><?php echo $brg['sku_brg']; ?></td>
            <td><?php echo rupiah($brg['harga_brg']); ?></td>
            <td>
              <button style="width: 85px;" class="btn btn-warning" onclick="edit(<?php echo $brg['id_brg'] ?>)"><i class="fa fa-pencil"></i> Edit</button>
              <button style="width: 85px;" class="btn btn-danger" onclick="hapus(<?php echo $brg['id_brg'] ?>)"><i class="fa fa-trash-o"></i> Hapus</button>
            </td>
          </tr>
        <?php endforeach ?>
        <tbody>
      </table>
    </div>
  </div>

  

<?php $this->load->view('header'); ?>



  <!-- Form Edit toko-->
  <div class="modal fade" id="modal_form">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h2 class="text-uppercase modal-title"></h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="container">
          <div class="modal-body form">
            <form id="form" class="form">
              <div class="form-body">
                <input id="id_brg" name="id_brg" type="hidden">
                <div class="form-group">
                  <label>Kategori:</label>
                  <select class="form-control" id="id_ktg" name="id_ktg" required="required" data-validation-required-message="Pilih Kategori" >
                    <option value="">Pilih Kategori</option>
                    <?php foreach ($kategori as $list): ?>
                    <option value="<?php echo $list['id_ktg'] ?>"><?php echo $list['nama_ktg'] ?></option>
                    <?php endforeach ?>
                  </select>
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Nama Barang:</label>
                  <input class="form-control" id="nama_brg" name="nama_brg" type="text" placeholder="Nama Barang *" required="required" data-validation-required-message=" Masukan Nama Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Deskripsi Barang:</label>
                  <textarea class="form-control" id="deskripsi_brg" name="deskripsi_brg" placeholder=" Deskripsi Barang *" required="required" data-validation-required-message="Masukan Deskripsi Barang."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Warna:</label>
                  <input class="form-control" id="warna_brg" name="warna_brg" type="text" placeholder="Warna Barang *" required="required" data-validation-required-message=" Masukan Warna Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Ukuran:</label>
                  <input class="form-control" id="ukuran_brg" name="ukuran_brg" type="text" placeholder="Ukuran Barang *" required="required" data-validation-required-message=" Masukan Ukuran Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Stok:</label>
                  <input class="form-control" id="stok_brg" name="stok_brg" type="number" placeholder="Stok Barang *" required="required" data-validation-required-message=" Masukan stok Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >SKU:</label>
                  <input class="form-control" id="sku_brg" name="sku_brg" type="text" placeholder="SKU Barang *" required="required" data-validation-required-message=" Masukan SKU Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Harga:</label>
                  <input class="form-control" id="harga_brg" name="harga_brg" type="number" placeholder="Harga Barang *" required="required" data-validation-required-message=" Masukan Harga Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label for="foto_file">Gambar</label><br />
                  <input id="foto_brg" name="foto_brg" type="hidden">
                  <input id="foto_file" name="file" type="file" multiple>
                  <div id="foto_file-preview"></div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              </div>
            </form>
        </div>
        </div>
      </div>
    </div>
  </div>
  

<?php $this->load->view('footer'); ?>

<script type="text/javascript">

save_method = 'tambah';
window.baseUrl = '<?php echo base_url(); ?>';

$(function () {
  $('#example1').DataTable();
})

$.validator.addMethod("valueNotEquals",function(value, element, arg) {
  return arg !== value;
}, "Value must not equal arg.");

$(function () {
    'use strict';
    var url_foto = "<?php echo site_url('produk/upload_foto')?>";
    $('#foto_file').fileupload({
        url: url_foto,
        dataType: 'json',
        done: function (e, data) {
            window.foto = data.result.file;
            var imgPrev = '<img src="'+window.baseUrl+'/'+window.foto+'" style="margin-top: 20px;" width="300" />';
            $("#foto_file-preview").html(imgPrev);
            $("#foto_brg").val(window.foto);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function tambah()
{
    save_method = 'tambah';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Barang'); // Set Title to Bootstrap modal title
}

function edit(id)
{
    save_method = 'edit';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('produk/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#id_brg').val(data.id_brg);
            $('#id_ktg').val(data.id_ktg);
            $('#nama_brg').val(data.nama_brg);
            $('#deskripsi_brg').val(data.deskripsi_brg);
            $('#warna_brg').val(data.warna_brg);
            $('#ukuran_brg').val(data.ukuran_brg);
            $('#stok_brg').val(data.stok_brg);
            $('#sku_brg').val(data.sku_brg);
            $('#harga_brg').val(data.harga_brg);
            $('#foto_brg').val(data.foto_brg);
            window.foto = data.foto_brg;
            var imgPrev = '<img src="'+window.baseUrl+'/'+window.foto+'" style="margin-top: 20px;" width="300" />';
            $("#foto_file-preview").html(imgPrev);
            $('.modal-title').text('Edit Barang');
            $('#modal_form').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

$("#btnSave").click(function() {
    $("#form").submit();
});

$("#form").validate({
    rules: {
        id_ktg: "required",
        nama_brg: "required",
        deskripsi_brg: "required",
        warna_brg: "required",
        ukuran_brg: "required",
        stok_brg: "required",
        sku_brg: "required",
        harga_brg: "required",
        foto_brg: "required"
    },
    messages: {
        id_ktg: "Mohon pilih kategori barang",
        nama_brg: "Mohon isikan nama barang",
        deskripsi_brg: "Mohon isikan deskripsi barang",
        warna_brg: "Mohon isikan warna barang",
        ukuran_brg: "Mohon isikan ukuran barang",
        stok_brg: "Mohon isikan stok barang",
        sku_brg: "Mohon isikan sku barang",
        harga_brg: "Mohon isikan harga barang",
        foto_brg: "Mohon isikan foto barang"
    },
    submitHandler: function() {
      simpan();
    }
});

function simpan()
{
    $('#btnSave').text('menyimpan...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 

    if(save_method == 'tambah') {
        var url = "<?php echo site_url('produk/ajax_tambah')?>";
    } else {
        var url = "<?php echo site_url('produk/ajax_perbarui')?>";
    }
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data Berhasil disimpan',
                showConfirmButton: false,
                timer: 2000
              })
              setTimeout(function() {
                location.reload();
              }, 2000);
            }

            $('#btnSave').text('Daftar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          Swal.fire({
            position: 'center',
            type: 'error',
            title: 'Oops...',
            text: 'Error menambahkan data',
            timer: 5000
          })
          $('#btnSave').text('Daftar'); //change button text
          $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

var rupiah = $('#harga_brg').val();
$('#harga_brg').onkeyup = function() {
  rupiah.value = formatRupiah(this.value, 'Rp. ');
};

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix){
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split       = number_string.split(','),
  sisa        = split[0].length % 3,
  rupiah        = split[0].substr(0, sisa),
  ribuan        = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

</script>

