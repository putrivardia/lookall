<?php $this->load->view('header'); ?>

  <!-- form profil konsumen -->
  <section style="margin-top: 25px; " class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <?php if ($pembeli['foto_pembeli']<>''){ ?>
            <center>
              <img class="lazy" alt="" src="<?php echo base_url(); ?><?php echo $pembeli['foto_pembeli'] ?>" style="display: block;width: 155px; height: 155px;border-radius:50%;" >
            </center><br>
              
            <?php } else { ?>
            <center>
              <img class="lazy" alt="" src="<?php echo base_url(); ?>bahan/upload/images.png" style="display: block;width: 155px; height: 155px;border-radius:50%;" >
            </center><br>
            <?php } ?>
          <h3 class="section-heading text-uppercase"> Profilku</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form id="contactForm" name="sentMessage" novalidate="novalidate">
            <div class="row justify-content-center" >
              <div class="col-md-6">

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label style="color: white;">Nama </label>
                    </div>
                    <div class="col-md-6">
                      <label style="color: white;">: &nbsp;&nbsp;<?php echo $pembeli['nama_pembeli'] ?></label>
                    </div>
                  </div>
                  
              <!--     <input class="form-control" id="namapembeli" type="text" placeholder="Nama Lengkap *" required="required" data-validation-required-message=" Masukan Nama Lengkap">
                  <p class="help-block text-danger"></p> -->
                </div>

                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                      <label style="color: white;">Email </label>
                    </div>
                      <div class="col-md-6">
                      <label style="color: white;">: &nbsp;&nbsp;<?php echo $pembeli['email_pembeli'] ?></label>
                    </div>
                  </div> 
              <!--     <input class="form-control" id="emailpembeli" type="text" placeholder="Email *" required="required" data-validation-required-message="Masukan Email">
                  <p class="help-block text-danger"></p> -->
                </div>
              
                     <div class="form-group">
                      <div class="row">
                      <div class="col-md-6">
                      <label style="color: white;">Kelamin </label>
                    </div>
                      <div class="col-md-6">
                      <label style="color: white;">: &nbsp;&nbsp;<?php echo $pembeli['kelamin_pembeli'] ?></label>
                    </div> 
            <!--       <textarea class="form-control" id="alamatpembeli" placeholder=" Alamat *" required="required" data-validation-required-message="Masukan Alamat."></textarea>
                  <p class="help-block text-danger"></p> -->
                </div>
                </div>
                   
          
                 <div class="form-group">

                    <div class="row">
                      <div class="col-md-6">
                      <label style="color: white;">Username </label>
                    </div>
                      <div class="col-md-6">
                      <label style="color: white;">: &nbsp;&nbsp;<?php echo $pembeli['username'] ?></label>
                    </div> 
               <!--    <input class="form-control" id="usernamepembeli" type="text" placeholder="Username *" required="required" data-validation-required-message="Masukan Username">
                  <p class="help-block text-danger"></p> -->
                </div>
                </div>

                <div class="form-group">
                     <div class="row">
                      <div class="col-md-6">
                      <label style="color: white;">Kata Sandi </label>
                    </div>
                      <div class="col-md-6">
                      <label style="color: white;">: &nbsp;&nbsp;*****</label>
                    </div>  
              <!--     <input class="form-control" id="passwordpembeli" type="password" placeholder="Password*" required="required" data-validation-required-message=" Masukan Password.">
                  <p class="help-block text-danger"></p> -->
                </div>
                </div>

                

              </div>
           <!--    <div class="col-md-6">
                <div class="form-group">
                  <textarea class="form-control" id="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div> -->
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center">
                <div id="success"></div>
                <button type="button" id="sendMessageButton" class="btn btn-primary btn-l text-uppercase" onclick="edit()">Ubah</button>
              
              </div>


            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

  <!-- Form Edit pembeli-->
  <div class="modal fade" id="modal_edt_pembeli">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h2 class="text-uppercase">Form Edit Profil Pembeli</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="container">
          <div class="modal-body form">
            <form id="form" class="form">
              <div class="form-body">
                  <input id="id_pembeli" name="id_pembeli" value="<?php echo $pembeli['id_pembeli'] ?>" type="hidden">
                <div class="form-group">
                  <label >Nama Lengkap:</label>
                  <input class="form-control" id="nama_pembeli" name="nama_pembeli" value="<?php echo $pembeli['nama_pembeli'] ?>" type="text" placeholder="Nama Lengkap *" required="required" data-validation-required-message=" Masukan Nama Lengkap">
                  <p class="help-block text-danger"></p>
                </div>

                  <div class="form-group">
                  <label >Email :</label>
                  <input class="form-control" id="email_pembeli" name="email_pembeli" value="<?php echo $pembeli['email_pembeli'] ?>" type="email" placeholder="Email *" required="required" data-validation-required-message="Masukan Email">
                  <p class="help-block text-danger"></p>
                </div>

                <div class="form-group">
                  <label>Jenis Kelamin:</label>
                  <select class="form-control" id="kelamin_pembeli" name="kelamin_pembeli" required="required" data-validation-required-message="Pilih Jenis Kelamin" >
                    <option value="0">Pilih Jenis Kelamin</option>
                    <option value="laki-laki" <?php if ($pembeli['kelamin_pembeli'] == "laki-laki" ) echo 'selected' ; ?>>Laki-laki</option>
                    <option value="perempuan" <?php if ($pembeli['kelamin_pembeli'] == "perempuan" ) echo 'selected' ; ?>>Perempuan</option>
                  </select>
                  <p class="help-block text-danger"></p>
                </div>

                 <div class="form-group">
                  <label >Nomor Handphone :</label>
                  <input class="form-control" id="nohp_pembeli" name="nohp_pembeli" value="<?php echo $pembeli['nohp_pembeli'] ?>" type="text" placeholder="Nomor Handphone Pembeli *" required="required" data-validation-required-message=" Masukan Nomor Handphone Pembeli">
                  <p class="help-block text-danger"></p>
                </div>

                 <div class="form-group">
                  <label >Username :</label>
                  <input class="form-control" id="username" name="username" value="<?php echo $pembeli['username'] ?>" type="text" placeholder="Username *" required="required" data-validation-required-message="Masukan Username">
                  <p class="help-block text-danger"></p>
                </div>

                <div class="form-group">
                  <label >Password :</label>
                  <input class="form-control" id="password" name="password" type="password" placeholder="*****">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label for="foto_file">Foto Pembeli</label><br />
                  <input id="foto_pembeli" name="foto_pembeli" type="hidden">
                  <input id="foto_file" name="file" type="file" multiple>
                  <div id="foto_file-preview"></div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              </div>
            </form>
        </div>
        </div>
      </div>
    </div>
  </div>

  

<?php $this->load->view('footer'); ?>

<script type="text/javascript">

var fotopembeli = '<?php echo $pembeli['foto_pembeli'] ?>';
window.baseUrl = '<?php echo base_url(); ?>';
$('#foto_pembeli').val(fotopembeli);

$(function () {
    'use strict';
    var url_foto = "<?php echo site_url('konsumen_profil/upload_foto')?>";
    $('#foto_file').fileupload({
        url: url_foto,
        dataType: 'json',
        done: function (e, data) {
            window.foto = data.result.file;
            var imgPrev = '<img src="'+window.baseUrl+'/'+window.foto+'" style="margin-top: 20px;" width="300" />';
            $("#foto_file-preview").html(imgPrev);
            $("#foto_pembeli").val(window.foto);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function edit()
{
  $('#modal_edt_pembeli').modal('show'); 
}

$("#btnSave").click(function() {
    $("#form").submit();
});

$("#form").validate({
    rules: {
        nama_pembeli: "required",
        email_pembeli: "required",
        kelamin_pembeli: "required",
        nohp_pembeli: "required",
        username: "required"
    },
    messages: {
        nama_pembeli: "Mohon isikan nama lengkap anda",
        email_pembeli: "Mohon isikan email anda",
        kelamin_pembeli: "Mohon pilih jenis kelamin anda",
        nohp_pembeli: "Mohon isikan nomor handphone anda",
        username: "Mohon isikan username anda"
    },
    submitHandler: function() {
      simpan();
    }
});

function simpan()
{
    $('#btnSave').text('menyimpan...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url = "<?php echo site_url('konsumen_profil/ajax_perbarui')?>";
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data Berhasil disimpan',
                showConfirmButton: false,
                timer: 2000
              })
              setTimeout(function() {
                location.reload();
              }, 2000);
            }

            $('#btnSave').text('Daftar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          Swal.fire({
            position: 'center',
            type: 'error',
            title: 'Oops...',
            text: 'Error menambahkan data',
            timer: 5000
          })
          $('#btnSave').text('Daftar'); //change button text
          $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}
</script>
