<?php $this->load->view('header'); ?>


<section class="container">

  <!-- Kategori & menu toko Grid -->
  <section class="bg-light page-section" id="portfolio" style="margin-top:-145px;margin-bottom: -160px;">
    <div class="container">
      <br>
      <div class="row">
        <div class="col-md-12 col-sm-12 portfolio-item">
          <div class="row">
            <div class="col-lg-12 text-center">
              <h2 class="section-heading text-uppercase">Produk Toko <?php echo $this->session->userdata('toko') ?></h2>
              <div class="box-body">
                <button type="button" class="btn btn-primary btn-l text-uppercase" onclick="tambah()">Tambah Barang</button>       
              </div>
            </div>

            <?php foreach ($barang as $prod): ?>  
              <div class="col-md-3 col-sm-6 portfolio-item" style="cursor:pointer;">
                <a href="<?php echo base_url(); ?>produk/detail/<?php echo $prod['id_brg'] ?>">
                  <center>
                  <div class="portfolio-caption"><img class="lazy" alt="" src="<?php echo base_url(); ?><?php echo $prod['foto_brg'] ?>" style="width: 155px; height: 155px;" >
                    <h4><?php echo $prod['nama_brg'] ?></h4>
                  </div>
                  </center>
                </a>
              </div> 
            <?php endforeach ?>

          </div>




          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Form Edit toko-->
  <div class="modal fade" id="modal_form">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h2 class="text-uppercase modal-title"></h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="container">
          <div class="modal-body form">
            <form id="form" class="form">
              <div class="form-body">
                <input id="id_brg" name="id_brg" type="hidden">
                <div class="form-group">
                  <label>Kategori:</label>
                  <select class="form-control" id="id_ktg" name="id_ktg" required="required" data-validation-required-message="Pilih Kategori" >
                    <option value="">Pilih Kategori</option>
                    <?php foreach ($kategori as $list): ?>
                    <option value="<?php echo $list['id_ktg'] ?>"><?php echo $list['nama_ktg'] ?></option>
                    <?php endforeach ?>
                  </select>
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Nama Barang:</label>
                  <input class="form-control" id="nama_brg" name="nama_brg" type="text" placeholder="Nama Barang *" required="required" data-validation-required-message=" Masukan Nama Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Deskripsi Barang:</label>
                  <textarea class="form-control" id="deskripsi_brg" name="deskripsi_brg" placeholder=" Deskripsi Barang *" required="required" data-validation-required-message="Masukan Deskripsi Barang."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Warna:</label>
                  <input class="form-control" id="warna_brg" name="warna_brg" type="text" placeholder="Warna Barang *" required="required" data-validation-required-message=" Masukan Warna Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Ukuran:</label>
                  <input class="form-control" id="ukuran_brg" name="ukuran_brg" type="text" placeholder="Ukuran Barang *" required="required" data-validation-required-message=" Masukan Ukuran Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Stok:</label>
                  <input class="form-control" id="stok_brg" name="stok_brg" type="number" placeholder="Stok Barang *" required="required" data-validation-required-message=" Masukan stok Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >SKU:</label>
                  <input class="form-control" id="sku_brg" name="sku_brg" type="text" placeholder="SKU Barang *" required="required" data-validation-required-message=" Masukan SKU Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Harga:</label>
                  <input class="form-control" id="harga_brg" name="harga_brg" type="number" placeholder="Harga Barang *" required="required" data-validation-required-message=" Masukan Harga Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label for="foto_file">Gambar</label><br />
                  <input id="foto_brg" name="foto_brg" type="hidden">
                  <input id="foto_file" name="file" type="file" multiple>
                  <div id="foto_file-preview"></div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              </div>
            </form>
        </div>
        </div>
      </div>
    </div>
  </div>
 

<?php $this->load->view('footer'); ?>

<script type="text/javascript">
window.baseUrl = '<?php echo base_url(); ?>';

$.validator.addMethod("valueNotEquals",function(value, element, arg) {
  return arg !== value;
}, "Value must not equal arg.");

$(function () {
    'use strict';
    var url_foto = "<?php echo site_url('produk/upload_foto')?>";
    $('#foto_file').fileupload({
        url: url_foto,
        dataType: 'json',
        done: function (e, data) {
            window.foto = data.result.file;
            var imgPrev = '<img src="'+window.baseUrl+'/'+window.foto+'" style="margin-top: 20px;" width="300" />';
            $("#foto_file-preview").html(imgPrev);
            $("#foto_brg").val(window.foto);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function tambah()
{
    save_method = 'tambah';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Barang'); // Set Title to Bootstrap modal title
}

$("#btnSave").click(function() {
    $("#form").submit();
});

$("#form").validate({
    rules: {
        id_ktg: "required",
        nama_brg: "required",
        deskripsi_brg: "required",
        warna_brg: "required",
        ukuran_brg: "required",
        stok_brg: "required",
        sku_brg: "required",
        harga_brg: "required",
        foto_brg: "required"
    },
    messages: {
        id_ktg: "Mohon pilih kategori barang",
        nama_brg: "Mohon isikan nama barang",
        deskripsi_brg: "Mohon isikan deskripsi barang",
        warna_brg: "Mohon isikan warna barang",
        ukuran_brg: "Mohon isikan ukuran barang",
        stok_brg: "Mohon isikan stok barang",
        sku_brg: "Mohon isikan sku barang",
        harga_brg: "Mohon isikan harga barang",
        foto_brg: "Mohon isikan foto barang"
    },
    submitHandler: function() {
      simpan();
    }
});

function simpan()
{
    $('#btnSave').text('menyimpan...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 

    if(save_method == 'tambah') {
        var url = "<?php echo site_url('produk/ajax_tambah')?>";
    } else {
        var url = "<?php echo site_url('produk/ajax_perbarui')?>";
    }
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data Berhasil disimpan',
                showConfirmButton: false,
                timer: 2000
              })
              setTimeout(function() {
                location.reload();
              }, 2000);
            }

            $('#btnSave').text('Daftar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          Swal.fire({
            position: 'center',
            type: 'error',
            title: 'Oops...',
            text: 'Error menambahkan data',
            timer: 5000
          })
          $('#btnSave').text('Daftar'); //change button text
          $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}
</script>