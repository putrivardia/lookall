<?php $this->load->view('header'); 

function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
  return $hasil_rupiah;
 
}
?>
  <!-- form daftar -->
  <section style="margin-top: 25px; " class="page-section" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
            <?php if ($barang['foto_brg']<>''){ ?>
            <center>
              <img class="lazy" alt="" src="<?php echo base_url(); ?><?php echo $barang['foto_brg'] ?>" style="display: block;width: 600px;height: 400px;" >
            </center><br>
              
            <?php } else { ?>
            <center>
              <img class="lazy" alt="" src="<?php echo base_url(); ?>bahan/upload/images.png" style="display: block;width: 155px; height: 155px;border-radius:50%;" >
            </center><br>
            <?php } ?>
          <h3 class="section-heading"> <?php echo $barang['nama_brg'] ?></h3>
          <h5 class="section-heading"> <?php echo $barang['nama_toko'] ?></h5><br>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form id="contactForm" name="sentMessage" novalidate="novalidate">
            <div class="row justify-content-center" >
              <div class="col-md-8">
              
                  <div class="form-group" style="margin-bottom: 0px;">
                    <div class="row">
                      <div class="col-md-3">&nbsp;</div>
                      <div class="col-md-2">
                        <label style="color: white;">Harga </label>
                      </div>
                      <div class="col-md-6">
                        <label style="color: white;">: <?php echo rupiah($barang['harga_brg']) ?></label>
                      </div> 
                    </div>
                  </div>

                  <div class="form-group" style="margin-bottom: 0px;">
                      <div class="row">
                        <div class="col-md-3">&nbsp;</div>
                        <div class="col-md-2">
                          <label style="color: white;">Ukuran</label>
                        </div>
                        <div class="col-md-6">
                          <label style="color: white;">: <?php echo $barang['ukuran_brg'] ?></label>
                        </div> 
                      </div> 
                  </div> 

                  <div class="form-group" style="margin-bottom: 0px;">
                    <div class="row">
                      <div class="col-md-3">&nbsp;</div>
                      <div class="col-md-2">
                        <label style="color: white;">Warna </label>
                      </div>
                      <div class="col-md-6">
                        <label style="color: white;">: <?php echo $barang['warna_brg'] ?></label>
                      </div>  
                    </div> 
                  </div>

                  <div class="form-group" style="margin-bottom: 0px;">
                    <div class="row">
                      <div class="col-md-3">&nbsp;</div>
                      <div class="col-md-2">
                        <label style="color: white;">Stok </label>
                      </div>
                      <div class="col-md-6">
                        <label style="color: white;">: <?php echo $barang['stok_brg'] ?></label>
                      </div>  
                    </div> 
                  </div> 

                  <div class="form-group" style="margin-bottom: 0px;">
                    <div class="row">
                      <div class="col-md-3">&nbsp;</div>
                      <div class="col-md-2">
                        <label style="color: white;">Kategori </label>
                      </div>
                      <div class="col-md-6">
                        <label style="color: white;">: <?php echo $barang['nama_ktg'] ?></label>
                      </div>  
                    </div> 
                  </div> 

                  <div class="form-group" style="margin-bottom: 0px;">
                     <div class="row">
                      <div class="col-md-3">&nbsp;</div>
                      <div class="col-md-2">
                        <label style="color: white;">SKU </label>
                      </div>
                      <div class="col-md-6">
                        <label style="color: white;">: <?php echo $barang['sku_brg'] ?></label>
                      </div> 
                     </div>
                  </div>

                  <div class="form-group" style="margin-bottom: 0px;">
                    <div class="row">
                      <div class="col-md-3">&nbsp;</div>
                      <div class="col-md-2">
                        <label style="color: white;">Deskripsi</label>
                      </div>
                      <div class="col-md-6">
                        <label style="color: white;">: <?php echo $barang['deskripsi_brg'] ?> </label>
                      </div>  
                    </div>
                  </div>

                  <?php if ($this->session->userdata("status")=="login_pembeli"): ?>
                  <div class="form-group" style="margin-bottom: 0px;">
                    <div class="row">
                      <div class="col-md-3">&nbsp;</div>
                      <div class="col-md-2">
                        <label style="color: white;">Jumlah</label>
                      </div>
                      <div class="col-md-6">
                        <label style="color: white;">: <input style="width: 100px;" type="number" name="qty" id="qty" value="1"> </label>
                      </div>  
                    </div>
                  </div>
                  <hr>
                  <div class="form-group" style="margin-bottom: 0px;">
                    <div class="row">
                      <div class="col-md-3">&nbsp;</div>
                      <div class="col-md-2">
                        <label style="color: white;">&nbsp;</label>
                      </div>
                      <div class="col-md-6">
                        <button onclick="kekeranjang(<?php echo $barang['id_brg'] ?>)" style="width:75px;margin-left:5px;" type="button" id="tambahkekeranjang" name="tambahkekeranjang" class="btn btn-primary btn-l text-uppercase"><i class="fa fa-cart-plus"></i></button>
                      </div>  
                    </div>
                  </div>
                  <?php endif ?>

              </div>
              <?php if ($this->session->userdata("status")=="login_toko"): ?>
              <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button type="button" id="sendMessageButton" class="btn btn-primary btn-l text-uppercase" onclick="edit(<?php echo $barang['id_brg'] ?>)">Ubah</button>

                  <button style="border-color: red;background-color: red;color: white;" type="button" class="btn btn-warning btn-l text-uppercase" onclick="hapus(<?php echo $barang['id_brg'] ?>)">Hapus</button>
  
              </div>
              <?php endif ?>

              </div>
          </form>
        </div>
      </div>
    </div>
  </section>

  <!-- Form Edit Barang-->
  <div class="modal fade" id="modal_form">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h2 class="text-uppercase modal-title"></h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="container">
          <div class="modal-body form">
            <form id="form" class="form">
              <div class="form-body">
                <input id="id_brg" name="id_brg" type="hidden">
                <div class="form-group">
                  <label>Kategori:</label>
                  <select class="form-control" id="id_ktg" name="id_ktg" required="required" data-validation-required-message="Pilih Kategori" >
                    <option value="">Pilih Kategori</option>
                    <?php foreach ($kategori as $list): ?>
                    <option value="<?php echo $list['id_ktg'] ?>"><?php echo $list['nama_ktg'] ?></option>
                    <?php endforeach ?>
                  </select>
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Nama Barang:</label>
                  <input class="form-control" id="nama_brg" name="nama_brg" type="text" placeholder="Nama Barang *" required="required" data-validation-required-message=" Masukan Nama Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Deskripsi Barang:</label>
                  <textarea class="form-control" id="deskripsi_brg" name="deskripsi_brg" placeholder=" Deskripsi Barang *" required="required" data-validation-required-message="Masukan Deskripsi Barang."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Warna:</label>
                  <input class="form-control" id="warna_brg" name="warna_brg" type="text" placeholder="Warna Barang *" required="required" data-validation-required-message=" Masukan Warna Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Ukuran:</label>
                  <input class="form-control" id="ukuran_brg" name="ukuran_brg" type="text" placeholder="Ukuran Barang *" required="required" data-validation-required-message=" Masukan Ukuran Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Stok:</label>
                  <input class="form-control" id="stok_brg" name="stok_brg" type="number" placeholder="Stok Barang *" required="required" data-validation-required-message=" Masukan stok Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >SKU:</label>
                  <input class="form-control" id="sku_brg" name="sku_brg" type="text" placeholder="SKU Barang *" required="required" data-validation-required-message=" Masukan SKU Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label >Harga:</label>
                  <input class="form-control" id="harga_brg" name="harga_brg" type="number" placeholder="Harga Barang *" required="required" data-validation-required-message=" Masukan Harga Barang">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <label for="foto_file">Gambar</label><br />
                  <input id="foto_brg" name="foto_brg" type="hidden">
                  <input id="foto_file" name="file" type="file" multiple>
                  <div id="foto_file-preview"></div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              </div>
            </form>
        </div>
        </div>
      </div>
    </div>
  </div>


 
  

<?php $this->load->view('footer'); ?>


<script type="text/javascript">

window.baseUrl = '<?php echo base_url(); ?>';

$(function () {
    'use strict';
    var url_foto = "<?php echo site_url('produk/upload_foto')?>";
    $('#foto_file').fileupload({
        url: url_foto,
        dataType: 'json',
        done: function (e, data) {
            window.foto = data.result.file;
            var imgPrev = '<img src="'+window.baseUrl+'/'+window.foto+'" style="margin-top: 20px;" width="300" />';
            $("#foto_file-preview").html(imgPrev);
            $("#foto_brg").val(window.foto);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function edit(id)
{
    save_method = 'edit';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('produk/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#id_brg').val(data.id_brg);
            $('#id_ktg').val(data.id_ktg);
            $('#nama_brg').val(data.nama_brg);
            $('#deskripsi_brg').val(data.deskripsi_brg);
            $('#warna_brg').val(data.warna_brg);
            $('#ukuran_brg').val(data.ukuran_brg);
            $('#stok_brg').val(data.stok_brg);
            $('#sku_brg').val(data.sku_brg);
            $('#harga_brg').val(data.harga_brg);
            $('#foto_brg').val(data.foto_brg);
            window.foto = data.foto_brg;
            var imgPrev = '<img src="'+window.baseUrl+'/'+window.foto+'" style="margin-top: 20px;" width="300" />';
            $("#foto_file-preview").html(imgPrev);
            $('.modal-title').text('Edit Barang');
            $('#modal_form').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

$("#btnSave").click(function() {
    $("#form").submit();
});

$("#form").validate({
    rules: {
        id_ktg: "required",
        nama_brg: "required",
        deskripsi_brg: "required",
        warna_brg: "required",
        ukuran_brg: "required",
        stok_brg: "required",
        sku_brg: "required",
        harga_brg: "required",
        foto_brg: "required"
    },
    messages: {
        id_ktg: "Mohon pilih kategori barang",
        nama_brg: "Mohon isikan nama barang",
        deskripsi_brg: "Mohon isikan deskripsi barang",
        warna_brg: "Mohon isikan warna barang",
        ukuran_brg: "Mohon isikan ukuran barang",
        stok_brg: "Mohon isikan stok barang",
        sku_brg: "Mohon isikan sku barang",
        harga_brg: "Mohon isikan harga barang",
        foto_brg: "Mohon isikan foto barang"
    },
    submitHandler: function() {
      simpan();
    }
});

function simpan()
{
    $('#btnSave').text('menyimpan...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 

    var url = "<?php echo site_url('produk/ajax_perbarui')?>";
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data Berhasil disimpan',
                showConfirmButton: false,
                timer: 2000
              })
              setTimeout(function() {
                location.reload();
              }, 2000);
            }

            $('#btnSave').text('Sipan'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          Swal.fire({
            position: 'center',
            type: 'error',
            title: 'Oops...',
            text: 'Error menambahkan data',
            timer: 5000
          })
          $('#btnSave').text('Sipan'); //change button text
          $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function hapus(id)
{
  Swal.fire({
    title: 'Apakah anda yakin akan menghapus data ini ?',
    text: "Data yang sudah di hapus tidak dapat dikembalikan lagi",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Tidak'
  }).then((result) => {
    if (result.value) {
      $.ajax({
          url : "<?php echo site_url('produk/ajax_hapus')?>/"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data berhasil dihapus',
                showConfirmButton: false,
                timer: 2000
              })
              location.replace(window.baseUrl+'produk');
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              Swal.fire({
                position: 'center',
                type: 'error',
                title: 'Oops...',
                text: 'Error menghapus data',
                timer: 5000
              })
          }
      });
    }
  })
}

function kekeranjang(id)
{
    $.ajax({
        url : "<?php echo site_url('belanja/ajax_kekeranjang')?>/"+id,
        type: "POST",
        data: {
          id : id,
          qty : $('#qty').val()
        },
        dataType: "JSON",
        success: function(data)
        {

            if(data.status)
            {
              Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Data Berhasil disimpan',
                showConfirmButton: false,
                timer: 2000
              })
              setTimeout(function() {
                location.replace('<?php echo site_url('belanja')?>');
              }, 2000);
            }


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          Swal.fire({
            position: 'center',
            type: 'error',
            title: 'Oops...',
            text: 'Error menambahkan data',
            timer: 5000
          })

        }
    });
}

</script>

