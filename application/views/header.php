<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>LOOKALL</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>bahan/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>bahan/css/dataTables.bootstrap4.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="<?php echo base_url(); ?>bahan/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="<?php echo base_url(); ?>bahan/css/agency.min.css" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url() ?>bahan/plugins/upload/jquery.fileupload.css">

  <link rel="stylesheet" href="<?php echo base_url() ?>bahan/plugins/sweetalert/sweetalert2.min.css">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" style="background-color: black;" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">LOOKALL</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ml-auto">

        <?php if ($this->session->userdata("status")=="login_pembeli"){ ?>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>beranda">Beranda</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>belanja">Belanja</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>tentang">Tentang</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>konsumen_profil">Akunku</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>riwayat">riwayat</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>konsumen_keranjang"><i class="fa fa-cart-plus"></i></a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>masuk/logout">Keluar</a>
          </li>

        <?php } elseif ($this->session->userdata("status")=="login_toko") { ?>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>beranda">Beranda</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>tentang">Tentang</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>toko_profil">Tokoku</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>pemesanan">Pemesanan</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>produk">Produk</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>laporan_toko">Laporan</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>masuk/logout">Keluar</a>
          </li>

        <?php } else { ?>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>beranda">Beranda</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>belanja">Belanja</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>tentang">Tentang</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url(); ?>masuk">Masuk</a>
          </li>
        <?php } ?>
        </ul>
      </div>
    </div>
  </nav>
  <style type="text/css">
    .error{
      color: red;
      font-size: 18px;
    }
  </style>

<?php 
//   function rupiah($angka){
  
//     $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
//     return $hasil_rupiah;

//   }
?>