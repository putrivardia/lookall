<?php $this->load->view('header'); 
function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
  return $hasil_rupiah;
 
}
?>

  <!-- Keranjang toko Grid -->
  <section class="bg-light page-section" id="portfolio">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Keranjang Saya</h2>
       </div>
      </div>
      <div class="row">
        <table id="example" class="table table-striped table-bordered">
          <thead>
              <tr>
                  <th>No</th>
                  <th>Foto</th>
                  <th>Barang</th>
                  <th>Jumlah Pesanan</th>
                  <th>Harga</th>
                  <th><input type="checkbox" id="checkAll"> Aksi</th>
              </tr>
          </thead>
          <tbody>
            <?php $no = 1; foreach ($keranjangku as $list): ?>
              <tr>
                  <td><?php echo $no++; ?></td>
                  <td><img class="lazy" src="<?php echo base_url(); ?><?php echo $list['foto_brg'] ?>" style="display: block;width: 100px; height: 100px;" ></td>
                  <td>
                    <table>
                      <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><?php echo $list['nama_brg'] ?></td>
                      </tr>
                      <tr>
                        <td>Warna</td>
                        <td>:</td>
                        <td><?php echo $list['warna_brg'] ?></td>
                      </tr>
                      <tr>
                        <td>SKU</td>
                        <td>:</td>
                        <td><?php echo $list['sku_brg'] ?></td>
                      </tr>
                      <tr>
                        <td>Harga</td>
                        <td>:</td>
                        <td><?php echo rupiah($list['harga_brg']) ?></td>
                      </tr>
                    </table>
                  </td>
                  <td><?php echo $list['quantity'] ?></td> 
                  <td><?php $tot = $list['harga_brg']*$list['quantity']; echo rupiah($tot);?></td>
                  <td><input type="checkbox" class="cbx_checkout" value="<?php echo $list['id_keranjang'] ?>" name="idcheckout" id="idcheckout" data-price="<?php echo $tot ?>"></td>
              </tr>
            <?php endforeach ?>
              <tr>
                  <td colspan="4">Total</td>
                  <td colspan="2"><div id="totcheck"></div></td>
              </tr>
          </tbody>
          <tfoot>
               
          </tfoot>
        </table>
        <div class="col-lg-12 text-right">
          <button type="button" class="btn btn-primary btn-l text-uppercase" onclick="checkout()">Checkout</button>
        
        </div>
      </div>
    </div>
  </section>
  

<?php $this->load->view('footer'); ?>

<script type="text/javascript">

$("#checkAll").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
    calc();
});

function calc() {
  var tots = 0;
  $(".cbx_checkout:checked").each(function() {
    var price = $(this).attr("data-price");
    tots += parseFloat(price);
  });
  window.totalharga = tots;
  var rpnya = formatRupiah(tots.toFixed(), 'Rp. ');
  $('#totcheck').html(rpnya);
}
$(function() {
  $(document).on("change", ".cbx_checkout", calc);
  calc();
});

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix){
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split       = number_string.split(','),
  sisa        = split[0].length % 3,
  rupiah        = split[0].substr(0, sisa),
  ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
 
  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }
 
  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function checkout()
{
  arridkranjang = [];
    $("input[name='idcheckout']:checked").each(function() {
      arridkranjang.push(this.value);
    });

    count = 0;
    for(x in arridkranjang) {
      count++;
    }

    if (count>0) {
      daftaridkranjng = arridkranjang.toString();
      Swal.fire({
        title: 'Apakah anda yakin akan melakukan checkout ?',
        text: "Data yang sudah di checkout akan masuk ke menu riwayat",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then((result) => {
        if (result.value) {
          $.ajax({
              url : "<?php echo site_url('konsumen_keranjang/pemesanan')?>",
              type: "POST",
              dataType: "JSON",
              data: {
                listID: daftaridkranjng
                ,totalharga: window.totalharga
              },
              success: function(data)
              {
                Swal.fire({
                  position: 'center',
                  type: 'success',
                  title: 'Anda berhasil melakukan checkout',
                  showConfirmButton: false,
                  timer: 2000
                });
                setTimeout(function() {
                  location.reload();
                }, 2000);
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  Swal.fire({
                    position: 'center',
                    type: 'error',
                    title: 'Oops...',
                    text: 'Error melakukan checkout',
                    timer: 5000
                  })
              }
          });
        }
      })
    } else {
      Swal.fire({
        position: 'center',
        type: 'error',
        title: 'Oops...',
        text: 'Error silahkan pilih data yang akan di checkout',
        timer: 5000
      })
    }
}
</script>

