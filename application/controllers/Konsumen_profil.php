<?php 
 
class Konsumen_profil extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array('M_pembeli'));
		$this->load->library('htmlcut');
	}
 
	function index() {
		if (!$this->session->userdata("status")) {
			redirect('masuk');
		}
		$id 	= $this->session->userdata("ID");
		$pembeli 	= (array)$this->M_pembeli->ambil_by_id($id);
    	$data = array(
			'pembeli' => $pembeli
		);
		$this->parser->parse('konsumen_profil', $data); #ini mengARAH KE VIEW LOGIN
	}

	public function upload_foto()
	{
		$img = rand(1000,100000)."_".$_FILES['file']['name'];
		$img_loc = $_FILES['file']['tmp_name'];
		$folder="bahan/upload/pembeli/";

		if(move_uploaded_file($img_loc,$folder.$img)) {
			$isi = $folder.$img;
			echo json_encode(['file'=>$isi]);
		} else {
			die("Gagal upload file");
		}
  	}

  	public function ajax_perbarui()
	{
		$nama_pembeli = $this->input->post('nama_pembeli');
		$email_pembeli = $this->input->post('email_pembeli');
		$kelamin_pembeli = $this->input->post('kelamin_pembeli');
		$nohp_pembeli = $this->input->post('nohp_pembeli');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$foto_pembeli = $this->input->post('foto_pembeli');
		if ($password=='') {
			$data = array(
				'nama_pembeli' => $nama_pembeli,
				'email_pembeli' => $email_pembeli,
				'kelamin_pembeli' => $kelamin_pembeli,
				'nohp_pembeli' => $nohp_pembeli,
				'foto_pembeli' => $foto_pembeli,
				'username' => $username
			);
		} else {
			$data = array(
				'nama_pembeli' => $nama_pembeli,
				'email_pembeli' => $email_pembeli,
				'kelamin_pembeli' => $kelamin_pembeli,
				'nohp_pembeli' => $nohp_pembeli,
				'foto_pembeli' => $foto_pembeli,
				'username' => $username,
				'password' => md5($password)
			);
		}
	    $this->M_pembeli->perbarui(array('id_pembeli' => (int) $this->input->post('id_pembeli')), $data);
		echo json_encode(array("status" => TRUE));
	}
  
}