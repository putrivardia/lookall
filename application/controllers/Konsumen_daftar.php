<?php 
 
class Konsumen_daftar extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array('M_pembeli'));
		$this->load->library('htmlcut');
	}
 
	function index() {

    $data = array(
			'image' => ''
		);
		$this->parser->parse('konsumen_daftar', $data);
	}

	public function ajax_tambah()
	{
		$data = array(
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
			'nama_pembeli' => $this->input->post('nama_pembeli'),
			'nohp_pembeli' => $this->input->post('nohp_pembeli'),
			'email_pembeli' => $this->input->post('email_pembeli'),
			'kelamin_pembeli' => $this->input->post('kelamin_pembeli'),
			'tgl_bergabung' => date('Y-m-d')
	    );
	    $insert = $this->M_pembeli->simpan($data);
			echo json_encode(array("status" => TRUE));
	}
  
}