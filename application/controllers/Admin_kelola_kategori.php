<?php 
 
class Admin_kelola_kategori extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array('M_kategori'));
		$this->load->library('htmlcut');
	}
 
	function index() {
		$kategori = $this->M_kategori->ambil_semua();
    	$data = array(
			'kategori' => $kategori
		);
		$this->parser->parse('admin/kelola_kategori', $data);
	}


	public function ajax_edit($id)
	{
		$data = $this->M_kategori->ambil_by_id($id);
		echo json_encode($data);
  	}

	public function ajax_simpan()
	{
	    $nama_ktg = $this->input->post('nama_ktg');
		$data = array(
			'nama_ktg' => $nama_ktg
	    );
	    $insert = $this->M_kategori->simpan($data);
			echo json_encode(array("status" => TRUE));
	}

  	public function ajax_update()
	{
		$nama_ktg = $this->input->post('nama_ktg');
		$data = array(
			'nama_ktg' => $nama_ktg
		);
	    $this->M_kategori->perbarui(array('id_ktg' => (int) $this->input->post('id_ktg')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_hapus($id)
	{
	    $this->M_kategori->hapus_by_id($id);
		echo json_encode(array("status" => TRUE));
	}


  
}