<?php 
 
class Admin_kelola_pembeli extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array('M_pembeli'));
		$this->load->library('htmlcut');
	}
 
	function index() {
		if (!$this->session->userdata("status")) {
			redirect('login');
		}
		$pembeli = $this->M_pembeli->ambil_semua();
		$data = array(
			'judul' => 'DATA PEMBELI',
			'subjudul' => 'Menu Data Pembeli digunakan untuk mengelola data Pembeli di aplikasi LOOKALL',
			'pembeli' => $pembeli
		);
		$this->parser->parse('admin/kelola_pembeli', $data);
	}

	public function ajax_hapus($id)
	{
	    $data = $this->M_pembeli->ambil_by_id($id);
	    if (file_exists($data->foto_pembeli)) {
	      unlink($data->foto_pembeli);
	    }
	    $this->M_pembeli->hapus_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
  
}