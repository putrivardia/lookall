<?php 
 
class Pemesanan extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array('M_toko','M_barang','M_kategori'));
		$this->load->library('htmlcut');
	}
 
	function index() {
    	$data = array(
			'image' => ''
		);
		$this->parser->parse('pemesanan', $data);
	}
  
}