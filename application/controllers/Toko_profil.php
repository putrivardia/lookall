<?php 
 
class Toko_profil extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array('M_toko'));
		$this->load->library('htmlcut');
	}
 
	function index() {
		if (!$this->session->userdata("status")) {
			redirect('masuk');
		}
		$id 	= $this->session->userdata("ID");
		$toko 	= (array)$this->M_toko->ambil_by_id($id);
    	$data 	= array(
			'toko' => $toko
		);
		$this->parser->parse('toko_profil', $data);
	}

	public function upload_foto()
	{
		$img = rand(1000,100000)."_".$_FILES['file']['name'];
		$img_loc = $_FILES['file']['tmp_name'];
		$folder="bahan/upload/toko/";

		if(move_uploaded_file($img_loc,$folder.$img)) {
			$isi = $folder.$img;
			echo json_encode(['file'=>$isi]);
		} else {
			die("Gagal upload file");
		}
  	}

  	public function ajax_perbarui()
	{
		$nama_toko = $this->input->post('nama_toko');
		$email_toko = $this->input->post('email_toko');
		$alamat_toko = $this->input->post('alamat_toko');
		$koordinat_toko = $this->input->post('koordinat_toko');
		$ig_toko = $this->input->post('ig_toko');
		$wa_toko = $this->input->post('wa_toko');
		$foto_toko = $this->input->post('foto_toko');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if ($password=='') {
			$data = array(
				'nama_toko' => $nama_toko,
				'email_toko' => $email_toko,
				'alamat_toko' => $alamat_toko,
				'koordinat_toko' => $koordinat_toko,
				'ig_toko' => $ig_toko,
				'wa_toko' => $wa_toko,
				'foto_toko' => $foto_toko,
				'username' => $username
			);
		} else {
			$data = array(
				'nama_toko' => $nama_toko,
				'email_toko' => $email_toko,
				'alamat_toko' => $alamat_toko,
				'koordinat_toko' => $koordinat_toko,
				'ig_toko' => $ig_toko,
				'wa_toko' => $wa_toko,
				'foto_toko' => $foto_toko,
				'username' => $username,
				'password' => md5($password)
			);
		}
	    $this->M_toko->perbarui(array('id_toko' => (int) $this->input->post('id_toko')), $data);
		echo json_encode(array("status" => TRUE));
	}
  
}