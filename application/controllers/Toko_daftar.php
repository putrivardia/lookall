<?php 
 
class Toko_daftar extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array('M_toko'));
		$this->load->library('htmlcut');
	}
 
	function index() {

    $data = array(
			'image' => ''
		);
		$this->parser->parse('toko_daftar', $data);
	}

	public function ajax_tambah()
	{
		$data = array(
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
			'nama_toko' => $this->input->post('nama_toko'),
			'email_toko' => $this->input->post('email_toko'),
			'alamat_toko' => $this->input->post('alamat_toko'),
			'koordinat_toko' => $this->input->post('koordinat_toko'),
			'tgl_bergabung' => date('Y-m-d')
	    );
	    $insert = $this->M_toko->simpan($data);
			echo json_encode(array("status" => TRUE));
	}
  
}