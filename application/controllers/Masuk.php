<?php 
 
class Masuk extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array('M_pembeli','M_toko'));
		$this->load->library('htmlcut');
	}
 
	function index() {

    $data = array(
			'image' => ''
		);
		$this->parser->parse('masuk', $data); #ini mengARAH KE VIEW LOGIN
	}

	function login(){
        $username = $this->input->post('username');
        $password = $this->input->post('katasandi');
        $where = array(
            'username' => $username,
            'password' => md5($password)
        );
        $cek = $this->M_pembeli->cek_login("tblpembeli", $where)->num_rows();
        $cek2 = $this->M_toko->cek_login("tbltoko", $where)->num_rows();
        if($cek > 0){
            $user_data = $this->M_pembeli->get_user_data("tblpembeli",$username);
            $data_session = array(
                'username' => $username,
                'ID' => $user_data->id_pembeli,
                'status' => "login_pembeli"
            );
            $this->session->set_userdata($data_session);
            redirect(base_url("konsumen_profil"));
            
        }else{
            if($cek2 > 0){
                $user_data = $this->M_toko->get_user_data("tbltoko",$username);
                $data_session = array(
                    'username' => $username,
                    'toko' => $user_data->nama_toko,
                    'ID' => $user_data->id_toko,
                    'status' => "login_toko"
                );
                $this->session->set_userdata($data_session);
                redirect(base_url("toko_profil"));
                
            }else{
                $this->session->set_flashdata('code','danger');
                $this->session->set_flashdata('msg','Gagal login, Username atau password salah '.$username.' '.$password);
                redirect(base_url('masuk'));
                // redirect(base_url('akunlogin'));
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('status');

        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert"> Logout Berhasil !!');
        redirect('beranda');
    }
  
}