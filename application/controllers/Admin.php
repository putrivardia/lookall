<?php 
 
class Admin extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array("M_user"));
		$this->load->library('htmlcut');
	}
 
	function index() {

    $data = array(
			'image' => ''
		);
		$this->parser->parse('admin', $data);
	}


	function login(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $where = array(
            'username' => $username,
            'password' => md5($password)
        );
        $cek = $this->M_user->cek_login("tbluser", $where)->num_rows();
        if($cek > 0){
            $user_data = $this->M_user->get_user_data("tbluser",$username);
            $data_session = array(
                'username' => $username,
                'tbluser_id' => $user_data->tbluser_id,
                'status' => "login"
            );
            $this->session->set_userdata($data_session);
            redirect(base_url("admin_dashboard"));
            
        }else{
            $this->session->set_flashdata('code','danger');
            $this->session->set_flashdata('msg','Gagal login, Username atau password salah');
            redirect(base_url('admin'));
            // redirect(base_url('akunlogin'));
        }
    }

  
}