<?php 
 
class Belanja extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array('M_kategori','M_barang','M_keranjang'));
		$this->load->library('htmlcut');
	}
 
	function index() {
		$kategori = $this->M_kategori->ambil_semua();
		$barang = $this->M_barang->ambil_semua();
    	$data = array(
			'barang' => $barang,
			'kategori' => $kategori
		);
		$this->parser->parse('belanja', $data);
	}

	public function tampil_barang($id)
	{
		$data = $this->M_barang->ambil_semua_by_id($id);
		$hasil="";
		foreach ($data as $datanya) {
			$hasil = $hasil.' 
                <div class="col-md-3 col-sm-6 portfolio-item">
                  <a href="'.base_url().'produk/detail/'.$datanya["id_brg"].'">
	              <div class="portfolio-caption" style="width: fit-content;"><img class="lazy" alt="" src="'.base_url().$datanya["foto_brg"].'" style="display: block;width: 155px; height: 155px;" >
	                <h4>'.$datanya["nama_brg"].'</h4>
	                <p class="text-muted">'.$datanya["nama_toko"].'</p>
	              </div>
	              </a>
	            </div>';
		}
		echo $hasil;
  	}

  	public function ajax_kekeranjang()
	{
	    $id = (int) $this->input->post('id');
	    $qty = (int) $this->input->post('qty');
		$data = array(
			'id_pembeli' => $this->session->userdata('ID'),
			'id_brg' => $id,
			'quantity' => $qty
	    );
	    $insert = $this->M_keranjang->simpan($data);
			echo json_encode(array("status" => TRUE));
	}
  
}