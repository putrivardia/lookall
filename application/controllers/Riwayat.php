<?php 
 
class Riwayat extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array('M_pemesanan','M_detailpemesanan','M_barang'));
		$this->load->library('htmlcut');
	}
 
	function index() {
		$pesanan = $this->M_detailpemesanan->ambil_semua_by_id_riwayat($this->session->userdata('ID'));
	    $data = array(
				'pesanan' => $pesanan
		);
		$this->parser->parse('riwayat', $data);
	}

	public function pemesanan()
	{
		$listID = trim($_REQUEST['listID'],','); 
		$totalharga = $_REQUEST['totalharga']; 

		$data = array(
			'id_pembeli' => $this->session->userdata('ID'),
			'totalharga_pemesanan' => $totalharga,
			'tanggal_pemesanan' => date('Y-m-d'),
			'status_pemesanan' => "menunggu konfirmasi"
	    );
	    $insert = $this->M_pemesanan->simpan($data);

	   	$getlastid = $this->M_pemesanan->ambilterakhir();

		$listID = explode(',', $listID);

		foreach ($listID as $id) {
			$this->MulaiTransfer($id,$getlastid);
		}
		echo json_encode(array("status" => TRUE));
	}

	public function MulaiTransfer($id,$idpemesanan)
	{
		$krnjng = $this->M_keranjang->ambil_by_id($id);
		$data = array(
			'id_pembeli' => $this->session->userdata('ID'),
			'id_brg' => $krnjng->id_brg,
			'id_pemesanan' => $idpemesanan,
			'quantity' => $krnjng->quantity
	    );
	    $insert = $this->M_detailpemesanan->simpan($data);
		$this->UpdateStok($krnjng->id_brg,$krnjng->quantity);
		$this->MulaiHapus($id);
	}

	public function MulaiHapus($id)
	{
	    $this->M_keranjang->hapus_by_id($id);
	}

	public function UpdateStok($idbrg,$qty)
	{
		$cekbrg = $this->M_barang->ambil_by_id($idbrg);
		$stok = $cekbrg->stok_brg-$qty;
		$data = array(
			'stok_brg' => $stok
		);
	    $this->M_barang->perbarui(array('id_brg' => $idbrg), $data);
	}

	public function ajax_batal()
	{
		$id = $_REQUEST['id'];
		$brg = $_REQUEST['brg'];
		$qty = $_REQUEST['qty'];
		$data = array(
			'status_pemesanan' => "dibatalkan"
		);
	    $this->M_pemesanan->perbarui(array('id_pemesanan' => $id), $data);

	    $cekbrg = $this->M_barang->ambil_by_id($brg);
		$stok = $cekbrg->stok_brg+$qty;
	    $data2 = array(
			'stok_brg' => $stok
		);
	    $this->M_barang->perbarui(array('id_brg' => $brg), $data2);


		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
	    $this->M_detailpemesanan->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
  
}