<?php 
 
class Admin_kelola_penjual extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array('M_toko'));
		$this->load->library('htmlcut');
	}
 
	function index() {
		if (!$this->session->userdata("status")) {
			redirect('login');
		}
		$penjual = $this->M_toko->ambil_semua();
		$data = array(
			'judul' => 'DATA PENJUAL',
			'subjudul' => 'Menu Data Penjual digunakan untuk mengelola data Penjual di aplikasi LOOKALL',
			'penjual' => $penjual
		);
		$this->parser->parse('admin/kelola_penjual', $data);
	}

	public function ajax_hapus($id)
	{
	    $data = $this->M_toko->ambil_by_id($id);
	    if (file_exists($data->foto_toko)) {
	      unlink($data->foto_toko);
	    }
	    $this->M_toko->hapus_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
  
}