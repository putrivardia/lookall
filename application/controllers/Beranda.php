<?php 
 
class Beranda extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array('M_toko'));
		$this->load->library('htmlcut');
	}
 
	function index() {
		$toko = $this->M_toko->ambil_semua();
    	$data = array(
			'toko' => $toko
		);
		$this->parser->parse('beranda', $data);
	}

	public function toko($id)
	{
		echo $id;
	}
  
}