<?php 
 
class Produk extends CI_Controller{
 
	function __construct() {
		parent::__construct();		
		$this->load->model(array('M_toko','M_barang','M_kategori'));
		$this->load->library('htmlcut');
	}
 
	function index() {
		$kategori = $this->M_kategori->ambil_semua();
		$barang = $this->M_barang->ambil_by_toko($this->session->userdata('ID'));
    	$data = array(
			'kategori' => $kategori,
			'barang' => $barang
		);
		$this->parser->parse('produk', $data);
	}

	public function upload_foto()
	{
		$img = rand(1000,100000)."_".$_FILES['file']['name'];
		$img_loc = $_FILES['file']['tmp_name'];
		$folder="bahan/upload/barang/";

		if(move_uploaded_file($img_loc,$folder.$img)) {
			$isi = $folder.$img;
			echo json_encode(['file'=>$isi]);
		} else {
			die("Gagal upload file");
		}
  	}

	public function ajax_tambah()
	{
		$data = array(
			'id_toko' => $this->session->userdata('ID'),
			'id_ktg' => $this->input->post('id_ktg'),
			'nama_brg' => $this->input->post('nama_brg'),
			'foto_brg' => $this->input->post('foto_brg'),
			'warna_brg' => $this->input->post('warna_brg'),
			'ukuran_brg' => $this->input->post('ukuran_brg'),
			'stok_brg' => $this->input->post('stok_brg'),
			'sku_brg' => $this->input->post('sku_brg'),
			'harga_brg' => $this->input->post('harga_brg'),
			'deskripsi_brg' => $this->input->post('deskripsi_brg')
	    );
	    $insert = $this->M_barang->simpan($data);
			echo json_encode(array("status" => TRUE));
	}

	public function ajax_edit($id)
	{
		$data = $this->M_barang->ambil_by_id($id);
		echo json_encode($data);
  	}

  	public function ajax_perbarui()
	{
		$data = array(
			'id_ktg' => $this->input->post('id_ktg'),
			'nama_brg' => $this->input->post('nama_brg'),
			'foto_brg' => $this->input->post('foto_brg'),
			'warna_brg' => $this->input->post('warna_brg'),
			'ukuran_brg' => $this->input->post('ukuran_brg'),
			'stok_brg' => $this->input->post('stok_brg'),
			'sku_brg' => $this->input->post('sku_brg'),
			'harga_brg' => $this->input->post('harga_brg'),
			'deskripsi_brg' => $this->input->post('deskripsi_brg')
		);
		
	    $this->M_barang->perbarui(array('id_brg' => (int) $this->input->post('id_brg')), $data);
		echo json_encode(array("status" => TRUE));
	}

  	public function detail($id)
	{
	    $barang = (array)$this->M_barang->ambil_by_id($id);
	    $kategori = $this->M_kategori->ambil_semua();
		$data = array(
			'barang' => $barang,
			'kategori' => $kategori
		);
		$this->parser->parse('toko_detailproduk', $data);
	}

	public function ajax_hapus($id)
	{
	    $data = $this->M_barang->ambil_by_id($id);
	    if (file_exists($data->foto_brg)) {
	      unlink($data->foto_brg);
	    }
	    $this->M_barang->hapus_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
  
}